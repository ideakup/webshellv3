<?php

return [

    // General Word
    
    //Form Element Word
    'signin' => 'Giriş Yap',
    'logout' => 'Çıkış',
    'resetpassword' => 'Şifreni Sıfırla',
    'changepassword' => 'Şifre Değiştir',

    'email' => 'E-posta',
    'password' => 'Şifre',
    'confirmpassword' => 'Şifre Tekrar',
    'rememberme' => 'Beni Hatırla',
    'forgetpassword' => 'Şifremi Unuttum ?',
    'anotheraccount' => 'Başka hesap ?',
    
    'request' => 'Gönder',
    'cancel' => 'Vazgeç',

    'emailaddress' => 'E-Mail Address',
    'emailtoreset' => 'Şifrenizi yenilemek için e-postanızı girin:',
    
    'sendresetlink' => 'Şifre Yenileme Bağlantısını Gönder',


    'menu' => 'Menü',
    'menulist' => 'Menü Listesi',
    'menuview' => 'Menü Görüntüle',
    'menuadd' => 'Menü Ekle',
    'menuedit' => 'Menü Düzenle',
    'menudelete' => 'Menü Sil',
    'menudetails' => 'Menü Detayları',
    'addnewmenuelement' => 'Yeni Menü Ekle',
    'topmenu' => 'Üst Menü',
    'cannotselectedtopmenu' => 'Üst Menü Seçilemez.',


    
    'contentlist' => 'İçerik Listesi',
    'contentview' => 'İçerik Görüntüle',
    'contentadd' => 'İçerik Ekle',
    'contentedit' => 'İçerik Düzenle',
    'contentdelete' => 'İçerik Sil',
    'contentdetails' => 'İçerik Detayları',
    'addnewcontent' => 'Yeni İçerik Ekle',
    


    'confirm' => 'Onay',
    'reject' => 'Red',
    'send' => 'Gönder',
    'save' => 'Kaydet',
    'reset' => 'Sıfırla',
    'edit' => 'Düzenle',
    'view' => 'Görüntüle',
    'type' => 'Tip',
    'close' => 'Kapat',
    'back' => 'Geri',
    'setting' => 'Ayarlar',
    'content' => 'İçerik',
    'contentlist' => 'İçerik Listesi',
    'title' => 'Başlık',


    'selectavalue' => 'Seçiniz...',
    'order' => 'Sıralama',
    'slug' => 'Slug',

    //Menu Word
    'dashboard' => 'Kontrol Paneli',
    'organizationscheme' => 'Organizasyon Şeması',
    'fullscheme' => 'Org. Şeması',
    'placement' => 'Atama',

    'department' => 'Departman',
    'departmentlist' => 'Departman Listesi',
    'departmentview' => 'Departman Görüntüle',
    'departmentadd' => 'Departman Ekle',
    'departmentedit' => 'Departman Düzenle',
    'departmentdelete' => 'Departman Sil',
    'departmentdetails' => 'Departman Detayları',

    'thisdepartmentdelete' => 'Bu Departmanı Sil',
    'thisdepartmentdeleted' => 'Depatman Silindi !!!',
    'topdepartment' => 'Üst Departman',
    'cannotselectedtopdepartment' => 'Üst Departman Seçilemez.',
    'addnewdeparment' => 'Yeni Depatman Ekle',
    
    
    'documentloader' => 'Dökümanı Yükleyen',
    'documentloadershort' => 'Dök. Yükleyen',

    'owndepartmentmanager' => 'Kendi Departman Yöneticisi',
    'owndepartmentmanagershort' => 'Kendi Dep. Yön.',
    'owndepartmentmanagerdescription' => ' ',
    
    'selectdepartmentorposition' => 'Departman yada Pozisyon Seçin',
    'selectdepartmentorpositiondescription' => ' ',

    'position' => 'Pozisyon',
    'positionlist' => 'Pozisyon Listesi',
    'positionview' => 'Pozisyon Görüntüle',
    'positionadd' => 'Pozisyon Ekle',
    'positionedit' => 'Pozisyon Düzenle',
    'positiondelete' => 'Pozisyon Sil',
    'positiondetails' => 'Pozisyon Detayları',

    'thispositiondelete' => 'Bu Pozisyonu Sil',
    'thispositiondeleted' => 'Pozisyon Silindi !!!',
    'topposition' => 'Üst Pozisyon',
    'cannotselectedtopposition' => 'Üst Pozisyon Seçilemez.',
    'addnewposition' => 'Yeni Pozisyon Ekle',
    
    
    'myprocesses' => 'Süreçlerim',
    'addnewprocess' => 'Yeni Süreç Ekle',
    'processtype' => 'Süreç Tipi',
    'follow-upprocess' => 'İlgili Süreçler',

    'processdetails' => 'Süreç Detayları',
    'processmanagement' => 'Süreç Yönetimi',
    'processmanagementlist' => 'Süreç Yönetimi Listesi',
    'viewprocess' => 'Süreç Görüntüle',
    'addprocess' => 'Süreç Ekle',
    'editprocess' => 'Süreç Düzenle',
    'deleteprocess' => 'Süreç Sil',
    'deletethisprocess' => 'Bu Süreci Sil',

    'adddeleteprocesssteps' => 'Süreç Adımı Ekle/Sil',
    'addprocessstep' => 'Süreç Adımı Ekle',
    'processsteplist' => 'Süreç Adımı Listesi',
    'editprocesssteps' => 'Süreç Adımı Düzenle',

    'selectstepdetail' => 'Süreç Adımı Detaylarını Seçin',
    'processowner' => 'Sahibi',


    'formgenerator' => 'Form Üreticisi',


    'formgroup' => 'Form Grubu',
    'formgroups' => 'Form Grupları',
    'formgrouplist' => 'Form Grubu Listesi',
    'formgroupdetails' => 'Form Grubu Detayları',

    'viewformgroup' => 'Form Grubu Görüntüle',
    'addformgroup' => 'Form Grubu Ekle',
    'editformgroup' => 'Form Grubu Düzenle',
    'deleteformgroup' => 'Form Grubu Sil',

    'addnewformgroup' => 'Yeni Form Grup Ekle',
    'deletethisformgroup' => 'Bu Form Grubunu Sil',


    'formelement' => 'Form Elemanı',
    'formelements' => 'Form Elemanları',
    'formelementlist' => 'Form Elemanı Listesi',
    'formelementdetails' => 'Form Elemanı Detayları',

    'viewformelement' => 'Form Elemanı Görüntüle',
    'addformelement' => 'Form Elemanı Ekle',
    'editformelement' => 'Form Elemanı Düzenle',
    'deleteformelement' => 'Form Elemanı Sil',
    
    'addnewformelement' => 'Yeni Form Elemanı Ekle',
    'deletethisformelement' => 'Bu Form Elemanını Sil',

    'addformelements' => 'Form Elemanları Ekle',

    'addnewplacement' => 'Yeni Atama Ekle',
    'deleteplacement' => 'Atama Sil',

    'user' => 'Kullanıcı',
    'userlist' => 'Kullanıcı Listesi',
    'viewuser' => 'Kullanıcı Görüntüle',
    'adduser' => 'Kullanıcı Ekle',
    'edituser' => 'Kullanıcı Düzenle',
    'deleteuser' => 'Kullanıcı Sil',
    'deletethisuser' => 'Bu Kullanıcıyı Sil',
    'userdetails' => 'Kullanıcı Detayı',
    'addnewuser' => 'Yeni Kullanıcı Ekle',


    'actions' => 'İşlemler',
    'description' => 'Açıklama',
    'status' => 'Durum',
    'date' => 'Tarih',
    'name' => 'Adı',
    'fullname' => 'Tam Adı',
    'createdate' => 'Tarih',
    'active' => 'Aktif',
    'passive' => 'Pasif',
    'detail' => 'Detay',
    'startpermission' => 'Başlatma Yetkisi',
    'all' => 'Herkes',
    'admin' => 'Yönetici',


    'detailsandconfirmation' => 'Detaylar ve Onay',
    'recentactivities' => 'Son Hareketler',

    'reasonmessage' => 'Sebep Mesajı',
    'confirm' => 'Onay',
    'reject' => 'Red',
    'send' => 'Gönder',
    'save' => 'Kaydet',
    'reset' => 'Sıfırla',
    'adddocument' => 'Döküman Ekle',
    'editdocument' => 'Döküman Düzenle',
    'edit' => 'Düzenle',
    'view' => 'Görüntüle',
    'type' => 'Tip',
    'close' => 'Kapat',
    'back' => 'Geri',

    'deleterecord' => 'Kaydı Sil',
    'deleting' => 'Siliyorsunuz...',
    'deletingmessage' => 'Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.',

    'addrecordsavechanges' => 'Add Record / Save changes',
    'recordid' => 'Kayıt No',

    'required' => 'Zorunlu Alan',
    'yes' => 'Evet',
    'no' => 'Hayır',
    'text' => 'Metin',
    'file' => 'Dosya',
    'amount' => 'Fiyat',
    'select' => 'Seçim',
    'value' => 'Değeri',
    'selectList' => 'Seçim Listesi',
    'editList' => 'Listeyi Düzenle',
    'addnewformelementselectlist' => 'Seçim Listesine Yeni Satır Ekle',

    'viewformelementselectlist' => 'Seçim Listesi Satırı Görüntüle',
    'addformelementselectlist' => 'Seçim Listesi Satırı Ekle',
    'editformelementselectlist' => 'Seçim Listesi Satırı Düzenle',
    'deleteformelementselectlist' => 'Seçim Listesi Satırı Sil',

    'detailformelementselectlist' => 'Seçim Listesi Satırı Detayları',

    'sendpassword' => 'Şifre Sıfırlama Postası Gönder',


    'sendpasswordmessage' => ' Adresine Şifre Resetleme E-postası Gönderildi.',

    'notdevupdate' => 'Geliştirici Kullanıcıların Bilgilerini Değiştiremezsiniz...',
    'successadd' => 'Başarıyla Eklendi...',
    'successedit' => 'Başarıyla Düzenlendi...',
    'successdelete' => 'Başarıyla Silindi...',
    'successchangepassword' => 'Şifre Başarıla Değiştirildi...',
    'successaddprocessstep' => 'Süreç Adımı Başarıla Eklendi...',


    'currency' => 'Para B.',
    'profileandsettings' => 'Profil ve Ayarlar',
    'profile' => 'Profil',

    //Special Word

];
