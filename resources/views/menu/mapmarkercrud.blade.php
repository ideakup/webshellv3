@extends('layouts.webshell')

@section('content') 

	<div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/content/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                İçerik Listesi @if(!is_null($menu))({{ $menu->variableLang($langs->first()->code)->name }})@endif
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            
                            @if (Request::segment(6) == 'add')
                                Harita - Kayıt Ekle
                            @elseif (Request::segment(6) == 'edit')
                                @if (is_null(Request::segment(8)))
                                    Harita - Kayıt Detayları
                                @else
                                    Harita - Kayıt Düzenle
                                @endif
                            @elseif (Request::segment(6) == 'delete')
                                Harita - Kayıt Sil
                            @endif
                            
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
	<div class="m-content">

		<div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
                            @if (Request::segment(6) == 'add')
                                Harita - Kayıt Ekle
                            @elseif (Request::segment(6) == 'edit')
                                @if (is_null(Request::segment(8)))
                                    Harita - Kayıt Detayları
                                @else
                                    Harita - Kayıt Düzenle
                                @endif
                            @elseif (Request::segment(6) == 'delete')
                                Harita - Kayıt Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                            @endif
						</h3>
					</div>
				</div>
                
				<div class="m-portlet__head-tools">
                    @if (Request::segment(6) == 'edit' && is_null(Request::segment(8)))
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/mapmarker/{{Request::segment(5)}}/delete/{{$staff->id}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Kayıt Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>

			</div>
            
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/'.Request::segment(3).'/mapmarker/'.Request::segment(5).'/save') }}" id="mapmarkerForm">
                
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(6) }}">
                <input type="hidden" name="menu_id" value="{{ (!is_null($menu)) ? $menu->id : '' }}">
                <input type="hidden" name="content_id" value="{{ (!is_null($content)) ? $content->id : '' }}">
                <input type="hidden" name="staff_id" value="{{ (!is_null($staff)) ? $staff->id : '' }}">

                <input type="hidden" name="lang" value="{{ Request::segment(8) }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">
                    
                    <div class="form-group m-form__group row">

                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">

                                @if (Request::segment(6) == 'add')
                                    Harita - Kayıt Ekle
                                @elseif (Request::segment(6) == 'edit')
                                    @if (is_null(Request::segment(8)))
                                        Harita - Kayıt Detayları
                                    @else
                                        Harita - Kayıt Düzenle
                                    @endif
                                @elseif (Request::segment(6) == 'delete')
                                    Harita - Kayıt Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                @endif

                            </h3>
                        </div>

                    </div>

                    <!-- BÖLGE MÜDÜRLÜĞÜ -->

                        @php

                            $_district_visible = true;
                            $_district_value = '';
                            $_district_disabled = '';

                            // $_district_value
                            if (Request::segment(6) != 'add' && empty(old('district'))) {
                                $_district_value = $staff->district_id;
                            } else {
                                $_district_value = old('district');
                            }

                            // $_district_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) == 'edit' && is_null(Request::segment(8)))){
                                $_district_disabled = ' disabled="disabled" ';
                            }else if(is_null($menu) && Request::segment(3) == 0) {
                                $_district_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_district_visible)

                            <div class="form-group m-form__group row @if ($errors->has('district')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Bölge Müdürlüğü
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('district'))
                                        <div id="type-error" class="form-control-feedback">{{ $errors->first('district') }}</div>
                                    @endif

                                    <select class="form-control m-select2" id="district" name="district" {!! $_district_disabled !!} >

                                        <option value="null">Seçiniz...</option>
                                            
                                        @foreach ($districts as $district)
                                            <option value="{{ $district->id }}" @if($_district_value == $district->id) {{ 'selected' }} @endif> {{ $district->variable->name }} </option>
                                        @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                            
                        @endif

                    <!-- BÖLGE MÜDÜRLÜĞÜ -->

                    <!-- İL -->

                        @php

                            $_city_visible = true;
                            $_city_value = '';
                            $_city_disabled = '';
                            
                            // $_city_value
                            if (Request::segment(6) != 'add' && empty(old('city'))) {
                                $_city_value = $staff->city_id;
                            } else {
                                $_city_value = old('city');
                            }

                            // $_city_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) == 'edit' && is_null(Request::segment(8)))){
                                $_city_disabled = ' disabled="disabled" ';
                            }else if(is_null($menu) && Request::segment(3) == 0) {
                                $_city_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_city_visible)

                            <div class="form-group m-form__group row @if ($errors->has('city')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    İl
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('city'))
                                        <div id="type-error" class="form-control-feedback">{{ $errors->first('city') }}</div>
                                    @endif

                                    <select class="form-control m-select2" id="city" name="city" {!! $_city_disabled !!} >

                                        <option value="null">Seçiniz...</option>
                                            
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}" @if($_city_value == $city->id) {{ 'selected' }} @endif> {{ $city->variable->name }} </option>
                                        @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                            
                        @endif

                    <!-- İL -->

                    <!-- İLÇE -->

                        @php

                            $_county_visible = true;
                            $_county_value = '';
                            $_county_disabled = '';
                            
                            // $_county_visible
                            if (Request::segment(6) != 'add' && empty(old('county'))) {
                                $_county_value = $staff->county_id;
                            } else {
                                $_county_value = old('county');
                            }

                            // $_county_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) == 'edit' && is_null(Request::segment(8)))){
                                $_county_disabled = ' disabled="disabled" ';
                            }else if(is_null($menu) && Request::segment(3) == 0) {
                                $_county_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_county_visible)

                            <div class="form-group m-form__group row @if ($errors->has('county')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    İlçe
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('county'))
                                        <div id="type-error" class="form-control-feedback">{{ $errors->first('county') }}</div>
                                    @endif

                                    <select class="form-control m-select2" id="county" name="county" {!! $_county_disabled !!} >
                                        <option value="null">Seçiniz...</option>
                                        @foreach ($counties as $county)
                                            <option value="{{ $county->id }}" @if($_county_value == $county->id) {{ 'selected' }} @endif> {{ $county->variable->name }} </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            
                        @endif

                    <!-- İLÇE -->
                    
                    <!-- ADI SOYADI -->
                        @php

                            $_name_visible = true;
                            $_name_value = '';
                            $_name_disabled = '';

                            // $_name_visible

                            // $_name_value
                            if (Request::segment(6) != 'add' && empty(old('name'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_name_value =  $staff->variableLang($langs->first()->code)->name;
                                } else {
                                    $_name_value = $staff->variableLang(Request::segment(8))->name;
                                }
                            } else {
                                $_name_value = old('name');
                            }

                            // $_name_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_name_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        <div class="form-group m-form__group row @if ($errors->has('name')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Adı Soyadı
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="name" name="name" value="{{ $_name_value }}" {!! $_name_disabled !!} required autofocus>
                                
                                @if ($errors->has('name'))
                                    <div id="name-error" class="form-control-feedback">{{ $errors->first('name') }}</div>
                                @endif

                            </div>
                        </div>
                    <!-- ADI SOYADI -->

                    
                    <!-- ÜNVANI -->

                        @php

                            $_ptitle_visible = true;
                            $_ptitle_value = '';
                            $_ptitle_disabled = '';
                            
                            // $_ptitle_visible

                            // $_ptitle_value
                            if (Request::segment(6) != 'add' && empty(old('ptitle'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_ptitle_value =  $staff->variableLang($langs->first()->code)->title;
                                } else {
                                    $_ptitle_value = $staff->variableLang(Request::segment(8))->title;
                                }
                            } else {
                                $_ptitle_value = old('ptitle');
                            }

                            // $_ptitle_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_ptitle_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_ptitle_visible)

                            <div class="form-group m-form__group row @if ($errors->has('ptitle')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Ünvanı
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('ptitle'))
                                        <div id="type-error" class="form-control-feedback">{{ $errors->first('ptitle') }}</div>
                                    @endif

                                    <select class="form-control m-select2" id="ptitle" name="ptitle" {!! $_ptitle_disabled !!} >

                                        <option value="null">Seçiniz...</option>

                                        @foreach (config('webshell.map_ptitle') as $key => $ptitle)
                                            <option value="{{ $ptitle['value'] }}" @if($_ptitle_value == $ptitle['value']) {{ 'selected' }} @endif> {{ $ptitle['title'] }} </option>
                                        @endforeach

                                    </select>
                                    
                                </div>
                            </div>
                            
                        @endif
                    <!-- ÜNVANI -->
 
                    
                    <!-- ADRES -->
                        @php

                            $_address_visible = true;
                            $_address_value = '';
                            $_address_disabled = '';

                            // $_address_visible

                            // $_address_value
                            if (Request::segment(6) != 'add' && empty(old('address'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_address_value =  $staff->variableLang($langs->first()->code)->address;
                                } else {
                                    $_address_value = $staff->variableLang(Request::segment(8))->address;
                                }
                            } else {
                                $_address_value = old('address');
                            }

                            // $_address_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_address_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        <div class="form-group m-form__group row @if ($errors->has('address')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Adres
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="address" name="address" value="{{ $_address_value }}" {!! $_address_disabled !!}>
                                
                                @if ($errors->has('address'))
                                    <div id="address-error" class="form-control-feedback">{{ $errors->first('address') }}</div>
                                @endif

                            </div>
                        </div>
                    <!-- ADRES -->

                    <!-- E-POSTA -->
                        @php

                            $_email_visible = true;
                            $_email_value = '';
                            $_email_disabled = '';

                            // $_email_visible

                            // $_email_value
                            if (Request::segment(6) != 'add' && empty(old('email'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_email_value =  $staff->variableLang($langs->first()->code)->email;
                                } else {
                                    $_email_value = $staff->variableLang(Request::segment(8))->email;
                                }
                            } else {
                                $_email_value = old('email');
                            }

                            // $_email_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_email_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        <div class="form-group m-form__group row @if ($errors->has('email')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                E-Posta
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="email" name="email" value="{{ $_email_value }}" {!! $_email_disabled !!}>
                                
                                @if ($errors->has('email'))
                                    <div id="email-error" class="form-control-feedback">{{ $errors->first('email') }}</div>
                                @endif

                            </div>
                        </div>
                    <!-- E-POSTA -->

                    <!-- TELEFON -->
                        @php

                            $_phone_visible = true;
                            $_phone_value = '';
                            $_phone_disabled = '';

                            // $_phone_visible

                            // $_phone_value
                            if (Request::segment(6) != 'add' && empty(old('phone'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_phone_value =  $staff->variableLang($langs->first()->code)->phone;
                                } else {
                                    $_phone_value = $staff->variableLang(Request::segment(8))->phone;
                                }
                            } else {
                                $_phone_value = old('phone');
                            }

                            // $_phone_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_phone_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        <div class="form-group m-form__group row @if ($errors->has('phone')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Telefon No
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="phone" name="phone" value="{{ $_phone_value }}" {!! $_phone_disabled !!}>
                                
                                @if ($errors->has('phone'))
                                    <div id="phone-error" class="form-control-feedback">{{ $errors->first('phone') }}</div>
                                @endif

                            </div>
                        </div>
                    <!-- TELEFON -->

                    <!-- GSM -->
                        @php

                            $_gsm_visible = true;
                            $_gsm_value = '';
                            $_gsm_disabled = '';

                            // $_gsm_visible

                            // $_gsm_value
                            if (Request::segment(6) != 'add' && empty(old('gsm'))) {
                                if(is_null($staff->variableLang(Request::segment(8)))){
                                    $_gsm_value =  $staff->variableLang($langs->first()->code)->gsm;
                                } else {
                                    $_gsm_value = $staff->variableLang(Request::segment(8))->gsm;
                                }
                            } else {
                                $_gsm_value = old('gsm');
                            }

                            // $_gsm_disabled
                            if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($staff->variableLang(Request::segment(8))))){
                                $_gsm_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        <div class="form-group m-form__group row @if ($errors->has('gsm')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                GSM No
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="gsm" name="gsm" value="{{ $_gsm_value }}" {!! $_gsm_disabled !!}>
                                
                                @if ($errors->has('gsm'))
                                    <div id="gsm-error" class="form-control-feedback">{{ $errors->first('gsm') }}</div>
                                @endif

                            </div>
                        </div>
                    <!-- GSM -->


                    <!-- ORDER -->
                        
                        @php

                            $_order_visible = false;
                            $_order_value = '';
                            $_order_disabled = '';
                            
                            // $_order_visible
                            if (Request::segment(6) == 'add' || (Request::segment(6) == 'edit' && is_null(Request::segment(8)))){
                                $_order_visible = true;
                            }
                            
                            // $_order_value
                            if (Request::segment(6) != 'add' && empty(old('order'))) {
                                $_order_value =  $staff->order;
                            } else {
                                $_order_value = old('order');
                            }

                            // $_order_disabled
                            if (Request::segment(6) == 'delete'){
                                $_order_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_order_visible)
                            <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Sıralama
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="number" min="1" max="1000000" id="order" name="order" value="{{ $_order_value }}" {!! $_order_disabled !!} required>
                                    
                                    @if ($errors->has('order'))
                                        <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                    @endif
                                </div>
                            </div>
                        @endif

                    <!-- ORDER -->

                    
                    <!-- STATUS -->
                        @php

                            $_status_visible = false;
                            $_status_checked = '';
                            $_status_disabled = '';
                            
                            // $_status_visible
                            if (Request::segment(6) == 'add' || (Request::segment(6) == 'edit' && is_null(Request::segment(8)))){
                                $_status_visible = true;
                            }

                            // $_status_checked
                            if (Request::segment(6) == 'add' || $staff->status == 'active') {
                                $_status_checked =  'checked="checked"';
                            }

                            // $_status_disabled
                            if (Request::segment(6) == 'delete'){
                                $_status_disabled = ' disabled="disabled" ';
                            }

                        @endphp

                        @if ($_status_visible)
                            <div class="m-form__group form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Pasif / Aktif
                                </label>
                                <div class="col-3">
                                    <span class="m-switch">
                                        <label>
                                            <input type="checkbox" id="status" name="status" value="active" {!! $_status_checked !!} {!! $_status_disabled !!} />
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        @endif
                    <!-- STATUS -->

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(6) == 'add' || Request::segment(6) == 'edit')

                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;

                                    @if(is_null($menu) && Request::segment(3) == 0)
                                        <a href="{{ url('form/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            Vazgeç
                                        </a>
                                    @else
                                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            Vazgeç
                                        </a>
                                    @endif

                                @elseif (Request::segment(6) == 'delete')

                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>

        @include('crudpartials.dropzone')

	</div>

@endsection


@section('inline-scripts')

    @include('crudpartials.contentjs')

    @include('crudpartials.formjs')

    @include('crudpartials.dropzonejs')

<script type="text/javascript">

    
    $(document).ready(function(){

        $('#district').select2({
            placeholder: "Seçiniz..."
        });
        $('#city').select2({
            placeholder: "Seçiniz..."
        });
        $('#county').select2({
            placeholder: "Seçiniz..."
        });
        $('#ptitle').select2({
            placeholder: "Seçiniz..."
        });



        
        @if (Request::segment(6) == 'edit' && !is_null(Request::segment(8)))
            @if ($content->type == 'text')
                /*
                    $('#row').select2({
                        placeholder: "Seçiniz..."
                    });
                */
            @endif
            @if ($content->type == 'form')
                var formBuilder = $('#fb-editor').formBuilder(options);
            @endif
        @endif

        @if (Request::segment(6) == 'edit' && is_null(Request::segment(8)))
            @if ($content->type == 'group' || $content->type == 'text' || $content->type == 'photogallery')
                $('#tag').select2({
                    placeholder: "Seçiniz..."
                });
                $('#category').select2({
                    placeholder: "Seçiniz..."
                });
            @endif

            @if ($content->type == 'photo' || $content->type == 'photogallery' || $content->type == 'seperator')
                $('#props').select2({
                    placeholder: "Seçiniz..."
                });
            @endif
        @endif

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    @if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($content->variableLang(Request::segment(8)))))
                        title: {
                            required: true
                        },
                    @endif
                    @if (Request::segment(6) == 'add')
                        type: {
                            required: true
                        },
                    @endif
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            @if (Request::segment(6) == 'edit' && !is_null(Request::segment(8)))
                @if ($content->type == 'form')
                    $('#formdata').val(formBuilder.actions.getData('json'));
                @endif
            @endif
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>

<style type="text/css">
    .m-portlet__body > .form-group{
        display: flex;
    }
</style>
@endsection
