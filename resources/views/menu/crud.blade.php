@extends('layouts.webshell')

@section('content')
@php
use Illuminate\Support\Facades\Input;
@endphp 

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Menü
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <a href="{{ url('menu/list') }}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Menü Listesi
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        @if (Request::segment(2) == 'add')
                        Menü Ekle
                        @elseif (Request::segment(2) == 'edit')
                        @if (is_null(Request::segment(4)))
                        Menü Ayarları
                        @else
                        Menü Düzenle
                        @endif
                        @elseif (Request::segment(2) == 'delete')
                        Menü Sil
                        @endif
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
  <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
           <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">
                 @if (Request::segment(2) == 'add')
                 Menü Ekle
                 @elseif (Request::segment(2) == 'edit')
                 @if (is_null(Request::segment(4)))
                 Menü Ayarları
                 @else
                 Menü Düzenle
                 @endif
                 @elseif (Request::segment(2) == 'delete')
                 Menü Sil
                 @endif
             </h3>
         </div>
     </div>
     <div class="m-portlet__head-tools">
        @if (Request::segment(2) == 'edit' && is_null(Request::segment(4)))
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                @if ($menu->subMenu->count() == 0)
                <a href="{{ url('menu/delete') }}/{{ $menu->id }}@if(!empty(Input::get('tmid')))?tmid={{ Input::get('tmid') }}@endif" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Menü'yü Sil">
                    <i class="fa fa-trash"></i>
                </a>
                @else
                <span class="m--font-danger">Alt Menü Mevcut Silinemez...</span>
                @endif
            </li>
        </ul>
        @endif
    </div>
</div>

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/save') }}" id="menuForm">
    {{ csrf_field() }}
    <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
    <input type="hidden" name="top_menu_id" value="{{ Input::get('tmid') }}">
    <input type="hidden" name="top_content_id" value="{{ Input::get('tcid') }}">
    <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
    <input type="hidden" name="lang" value="{{ Request::segment(4) }}">
    <div class="m-portlet__body">

        <div class="form-group m-form__group row">
            <div class="col-10 ml-auto">
                <h3 class="m-form__section">
                    @if (Request::segment(2) == 'add')
                    Menü Ekle
                    @elseif (Request::segment(2) == 'edit')
                    @if (is_null(Request::segment(4)))
                    Menü Ayarları
                    @else
                    [{{ Request::segment(4) }}] Menü Düzenle 
                    @endif
                    @elseif (Request::segment(2) == 'delete')
                    Menü Sil
                    @endif


                    @if ($menu->type == 'menuitem')
                    <span class="m-badge m-badge--brand m-badge--wide">Menü Öğesi (İçerik Yok)</span>
                    @elseif ($menu->type == 'content')
                    <span class="m-badge m-badge--brand m-badge--wide">İçerik</span>
                    @elseif($menu->type == 'photogallery')
                    <span class="m-badge m-badge--success m-badge--wide">Foto Galeri</span>
                    @elseif(starts_with($menu->type, 'list'))
                    <span class="m-badge m-badge--success m-badge--wide">Liste</span>
                    @elseif($menu->type == 'link')
                    <span class="m-badge m-badge--success m-badge--wide">Link (Yönlendirme)</span>
                    @endif
                </h3>
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('name')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Adı
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="name" name="name" 
                @if (Request::segment(2) != 'add' && empty(old('name')))
                value="@if (is_null($menu->variableLang(Request::segment(4)))){{ $menu->variableLang($langs->first()->code)->name }}@else{{ $menu->variableLang(Request::segment(4))->name }}@endif"
                @else 
                value="{{ old('name') }}" 
                @endif

                @if (Request::segment(2) == 'delete' || (Request::segment(2) == 'edit' && is_null(Request::segment(4))))
                disabled="disabled" 
                @endif
                required autofocus>
                @if ($errors->has('name'))
                <div id="name-error" class="form-control-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('slug')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Slug
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="slug" name="slug" 
                @if (Request::segment(2) != 'add' && empty(old('slug'))) 
                value="@if (is_null($menu->variableLang(Request::segment(4)))){{ $menu->variableLang($langs->first()->code)->slug }}@else{{ $menu->variableLang(Request::segment(4))->slug }}@endif"
                @else 
                value="{{ old('slug') }}" 
                @endif

                @if (Request::segment(2) == 'delete' ||  is_null(Request::segment(4)))
                disabled="disabled" 
                @endif
                required>
                @if ($errors->has('slug'))
                <div id="slug-error" class="form-control-feedback">{{ $errors->first('slug') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('menutitle')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Menü Başlığı
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="menutitle" name="menutitle" 
                @if (Request::segment(2) != 'add' && empty(old('menutitle'))) 
                value="@if (is_null($menu->variableLang(Request::segment(4)))){{ $menu->variableLang($langs->first()->code)->menutitle }}@else{{ $menu->variableLang(Request::segment(4))->menutitle }}@endif"
                @else 
                value="{{ old('menutitle') }}" 
                @endif

                @if (Request::segment(2) == 'delete' ||  (Request::segment(2) == 'edit' && is_null(Request::segment(4))))
                disabled="disabled" 
                @endif
                >
                @if ($errors->has('menutitle'))
                <div id="menutitle-error" class="form-control-feedback">{{ $errors->first('menutitle') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Sayfa Başlığı
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="title" name="title" 
                @if (Request::segment(2) != 'add' && empty(old('title'))) 
                value="@if (is_null($menu->variableLang(Request::segment(4)))){{ $menu->variableLang($langs->first()->code)->title }}@else{{ $menu->variableLang(Request::segment(4))->title }}@endif"
                @else 
                value="{{ old('title') }}" 
                @endif

                @if (Request::segment(2) == 'delete' ||  (Request::segment(2) == 'edit' && is_null(Request::segment(4))))
                disabled="disabled" 
                @endif
                >
                @if ($errors->has('title'))
                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                @endif
            </div>
        </div>

        @if ( (Request::segment(2) == 'edit' && is_null(Request::segment(4))) || Request::segment(2) == 'add')
        <hr>
        @endif

        @if (Request::segment(2) == 'add')
        <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Tip
            </label>
            <div class="col-7">
                @if ($errors->has('type'))
                <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
                @endif
                <select class="form-control m-select2" id="type" name="type"
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                >

                @foreach (config('webshell.menu_types') as $menutypes)
                @if($menutypes['visible'])
                <option value="{{ $menutypes['value'] }}" {{ ($menutypes['selected']) ? "selected" : "" }}> {{ $menutypes['title'] }} </option>
                @endif
                @endforeach

            </select>
        </div>
    </div>
    @endif

    @if ((empty(Input::get('tmid')) && empty(Input::get('tcid'))) && (Request::segment(2) == 'edit' && is_null(Request::segment(4))) || Request::segment(2) == 'add' )
    <div class="form-group m-form__group row @if ($errors->has('topmenu')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
    Üst Menü
    </label>
    <div class="col-7">
    @if ($errors->has('topmenu'))
    <div id="topmenu-error" class="form-control-feedback">{{ $errors->first('topmenu') }}</div>
    @endif
    <select class="form-control m-select2" id="topmenu" name="topmenu"
    @if (Request::segment(2) == 'delete')
    disabled="disabled" 
    @endif
    >
    <option value="null">Seçiniz...</option>
    @foreach ($topmenus as $topmenu)
    @if ($menu->id != $topmenu->id)
    <option value="{{ $topmenu->id }}"
        @if ($topmenu->id == old('topmenu') || $topmenu->id == $menu->top_id) 
        {{ 'selected' }} 
        @endif
        > {{ $topmenu->variable->name }} </option>
        @endif
        @endforeach
    </select>
</div>
</div>

<div class="form-group m-form__group row @if ($errors->has('description')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Açıklama
    </label>
    <div class="col-7">
        <textarea class="form-control m-input" id="description" name="description" rows="3"
        @if (Request::segment(2) == 'delete')
        disabled="disabled" 
        @endif
        >@if (Request::segment(2) != 'add' && empty(old('description'))){{ $menu->description }}@else{{ old('description') }}@endif</textarea>
        @if ($errors->has('description'))
        <div id="description-error" class="form-control-feedback">{{ $errors->first('description') }}</div>
        @endif
    </div>
</div>

<div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Sıralama
    </label>
    <div class="col-7">
        <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
        @if (Request::segment(2) != 'add' && empty(old('order'))) 
        value="{{ $menu->order }}"
        @else 
        value="{{ old('order') }}" 
        @endif

        @if (Request::segment(2) == 'delete')
        disabled="disabled" 
        @endif
        required autofocus>
        @if ($errors->has('order'))
        <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
        @endif
    </div>
</div>

<div class="form-group m-form__group row @if ($errors->has('position')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Pozisyon</label>
    <div class="col-7">
        <div class="m-radio-list">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="position" value="top" @if ($menu->position == 'top' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Sadece Üst Menüde Göster
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="position" value="aside" @if ($menu->position == 'aside') {{ 'checked="checked"' }} @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Sadece Yan/Ara Menüde Göster
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="position" value="all" @if ($menu->position == 'all') {{ 'checked="checked"' }} @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Her İkisinde de Göster
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="position" value="none" @if ($menu->position == 'none') {{ 'checked="checked"' }} @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Gösterme
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('position'))
    <div id="position-error" class="form-control-feedback">{{ $errors->first('position') }}</div>
    @endif
</div>

<div class="m-form__group form-group row">
    <label for="example-text-input" class="col-2 col-form-label">
        Pasif / Aktif
    </label>
    <div class="col-3">
        <span class="m-switch">
            <label>
                <input type="checkbox" @if ($menu->status == 'active' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                />
                <span></span>
            </label>
        </span>
    </div>
</div>
@endif

@if ((empty(Input::get('tmid')) && empty(Input::get('tcid'))) && Request::segment(2) == 'edit' && !is_null(Request::segment(4)))
@if($menu->type == 'link')
<hr>
<div class="form-group m-form__group row @if ($errors->has('stvalue_link')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Adres
    </label>
    <div class="col-7">
        <input class="form-control m-input" type="text" id="stvalue_link" name="stvalue_link" 
        @if (empty(old('stvalue_link')))
        @if (!is_null($menu->variableLang(Request::segment(4))))
        value="{{ json_decode($menu->variableLang(Request::segment(4))->stvalue, true)['link'] }}"
        @endif
        @else 
        value="{{ old('stvalue_link') }}" 
        @endif
        required autofocus>
        @if ($errors->has('stvalue_link'))
        <div id="stvalue_link-error" class="form-control-feedback">{{ $errors->first('stvalue_link') }}</div>
        @endif
    </div>
</div>

<div class="form-group m-form__group row @if ($errors->has('stvalue_target')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Sekme</label>
    <div class="col-7">
        <div class="m-radio-list">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="stvalue_target" value="self" @if (!is_null($menu->variableLang(Request::segment(4)))) @if (json_decode($menu->variableLang(Request::segment(4))->stvalue, true)['target'] == 'self' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Aynı Sekmede Aç
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="stvalue_target" value="blank" @if (!is_null($menu->variableLang(Request::segment(4)))) @if (json_decode($menu->variableLang(Request::segment(4))->stvalue, true)['target'] == 'blank') {{ 'checked="checked"' }} @endif @endif
                @if (Request::segment(2) == 'delete')
                disabled="disabled" 
                @endif
                > Yeni Sekmede Aç
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('stvalue_target'))
    <div id="stvalue_target-error" class="form-control-feedback">{{ $errors->first('stvalue_target') }}</div>
    @endif
</div>
@endif
@endif

@if ((empty(Input::get('tmid')) && empty(Input::get('tcid'))) && Request::segment(2) == 'edit' && is_null(Request::segment(4)))

@if($menu->type == 'link')
@else
<hr>
<div class="form-group m-form__group row @if ($errors->has('headertheme')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Üst Menü Stili</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="headertheme" value="light" @if ($menu->headertheme == 'light') {{ 'checked="checked"' }} @endif> Açık Ton
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="headertheme" value="dark" @if ($menu->headertheme == 'dark') {{ 'checked="checked"' }} @endif> Koyu Ton
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="headertheme" value="lightdark" @if ($menu->headertheme == 'lightdark') {{ 'checked="checked"' }} @endif> Açık -> Koyu Geçiş
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="headertheme" value="darklight" @if ($menu->headertheme == 'darklight') {{ 'checked="checked"' }} @endif> Koyu -> Açık Geçiş
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('headertheme'))
    <div id="headertheme-error" class="form-control-feedback">{{ $errors->first('headertheme') }}</div>
    @endif
</div>
<div class="form-group m-form__group row @if ($errors->has('slidertype')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Sayfa Görseli</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="slidertype" value="image" @if ($menu->slidertype == 'image') {{ 'checked="checked"' }} @endif> Fotoğraf
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="slidertype" value="slider" @if ($menu->slidertype == 'slider') {{ 'checked="checked"' }} @endif> Slider
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="slidertype" value="full-slider" @if ($menu->slidertype == 'full-slider') {{ 'checked="checked"' }} @endif> full Screen Slider
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="slidertype" value="no" @if ($menu->slidertype == 'no') {{ 'checked="checked"' }} @endif> Yok
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('headertheme'))
    <div id="headertheme-error" class="form-control-feedback">{{ $errors->first('headertheme') }}</div>
    @endif
</div>
<div class="form-group m-form__group row @if ($errors->has('breadcrumbvisible')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Navigasyon</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="breadcrumbvisible" value="yes" @if ($menu->breadcrumbvisible == 'yes') {{ 'checked="checked"' }} @endif> Var
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="breadcrumbvisible" value="no" @if ($menu->breadcrumbvisible == 'no') {{ 'checked="checked"' }} @endif> Yok
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('breadcrumbvisible'))
    <div id="breadcrumbvisible-error" class="form-control-feedback">{{ $errors->first('breadcrumbvisible') }}</div>
    @endif
</div>
<div class="form-group m-form__group row @if ($errors->has('asidevisible')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Yan/Ara Menü</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="asidevisible" value="yes" @if ($menu->asidevisible == 'yes') {{ 'checked="checked"' }} @endif> Var
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="asidevisible" value="no" @if ($menu->asidevisible == 'no') {{ 'checked="checked"' }} @endif> Yok
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('asidevisible'))
    <div id="asidevisible-error" class="form-control-feedback">{{ $errors->first('asidevisible') }}</div>
    @endif
</div>

@if ($menu->type == 'menuitem' && is_null($menu->top_id))
<div class="form-group m-form__group row @if ($errors->has('dropdowntype')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Sarkan Menü Türü</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="dropdowntype" value="normal" @if ($menu->dropdowntype == 'normal') {{ 'checked="checked"' }} @endif> Normal
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="dropdowntype" value="mega01" @if ($menu->dropdowntype == 'mega01') {{ 'checked="checked"' }} @endif> Mega Menü Tip 1
                <span></span>
            </label>

            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="dropdowntype" value="mega02" @if ($menu->dropdowntype == 'mega02') {{ 'checked="checked"' }} @endif> Mega Menü Tip 2
                <span></span>
            </label>
        </div>
    </div>
    @if ($errors->has('dropdowntype'))
    <div id="dropdowntype-error" class="form-control-feedback">{{ $errors->first('dropdowntype') }}</div>
    @endif
</div>
@endif
@endif

@if (starts_with($menu->type, 'list') || $menu->type == 'photogallery')
<div class="form-group m-form__group row @if ($errors->has('listtype')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Liste Tipi</label>
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="listtype" value="normal" @if ($menu->listtype == 'normal') {{ 'checked="checked"' }} @endif> Normal
                <span></span>
            </label>
                                        <!--
                                        <label class="m-radio m-radio--solid m-radio--state-brand">
                                            <input type="radio" name="listtype" value="headed" @if ($menu->listtype == 'headed') {{ 'checked="checked"' }} @endif> Başlıklı
                                            <span></span>
                                        </label>

                                        <label class="m-radio m-radio--solid m-radio--state-brand">
                                            <input type="radio" name="listtype" value="pyramid" @if ($menu->listtype == 'pyramid') {{ 'checked="checked"' }} @endif> Piramid
                                            <span></span>
                                        </label>
                                        <br>
                                    -->
                                    <label class="m-radio m-radio--solid m-radio--state-brand">
                                        <input type="radio" name="listtype" value="col-2x" @if ($menu->listtype == 'col-2x') {{ 'checked="checked"' }} @endif> 2 Kolon
                                        <span></span>
                                    </label>

                                    <label class="m-radio m-radio--solid m-radio--state-brand">
                                        <input type="radio" name="listtype" value="col-3x" @if ($menu->listtype == 'col-3x') {{ 'checked="checked"' }} @endif> 3 Kolon
                                        <span></span>
                                    </label>

                                    <label class="m-radio m-radio--solid m-radio--state-brand">
                                        <input type="radio" name="listtype" value="col-4x" @if ($menu->listtype == 'col-4x') {{ 'checked="checked"' }} @endif> 4 Kolon
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            @if ($errors->has('listtype'))
                            <div id="listtype-error" class="form-control-feedback">{{ $errors->first('listtype') }}</div>
                            @endif
                        </div>
                        @endif
                        @endif

                        @if ((!empty(Input::get('tmid')) || !empty(Input::get('tcid'))) && Request::segment(2) == 'edit' && is_null(Request::segment(4)))
                        @php 
                            //dump($menu->id);
                            //dump(Input::get('tmid'));
                        if(!empty(Input::get('tmid'))){
                            $top_has_sub = App\TopHasSub::where('top_menu_id', Input::get('tmid'))->where('sub_menu_id', $menu->id)->first();
                        }else{
                            $top_has_sub = App\TopHasSub::where('top_content_id', Input::get('tcid'))->where('sub_menu_id', $menu->id)->first();
                        }
                            //dump($top_has_sub);
                        @endphp
                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                @if (Request::segment(2) != 'add' && empty(old('order'))) 
                                value="{{ $top_has_sub->order }}"
                                @else 
                                value="{{ old('order') }}" 
                                @endif

                                @if (Request::segment(2) == 'delete')
                                disabled="disabled" 
                                @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('props_sum_type')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Özet Şablonu Tipi
                            </label>
                            <div class="col-7">
                                @if ($errors->has('props_sum_type'))
                                <div id="props_sum_type-error" class="form-control-feedback">{{ $errors->first('props_sum_type') }}</div>
                                @endif

                                

                                <select class="form-control m-select2" id="props_sum_type" name="props_sum_type"
                                @if (Request::segment(4) == 'delete')
                                disabled="disabled" 
                                @endif
                                >
                                @foreach (config('webshell.props.menu_default_sum_props.props_sum_type') as $key => $sec)
                                <option value="{{ $sec['value'] }}"
                                @if (!empty(json_decode($top_has_sub->props)->props_sum_type))
                                @if (json_decode($top_has_sub->props)->props_sum_type == $sec['value']) 
                                {{ 'selected' }} 
                                @endif
                                @endif
                                > {{ $sec['title'] }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('props_sum_count')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Özet Sütun Sayısı
                        </label>
                        <div class="col-7">
                            @if ($errors->has('props_sum_count'))
                            <div id="props_sum_count-error" class="form-control-feedback">{{ $errors->first('props_sum_count') }}</div>
                            @endif
                            <select class="form-control m-select2" id="props_sum_count" name="props_sum_count"
                            @if (Request::segment(4) == 'delete')
                            disabled="disabled" 
                            @endif
                            >
                            @foreach (config('webshell.props.menu_default_sum_props.props_sum_count') as $key => $sec)
                            <option value="{{ $sec['value'] }}"
                            @if (!empty(json_decode($top_has_sub->props)->props_sum_count))
                            @if (json_decode($top_has_sub->props)->props_sum_count == $sec['value']) 
                            {{ 'selected' }} 
                            @endif
                            @endif
                            > {{ $sec['title'] }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group m-form__group row @if ($errors->has('props_sum_colvalue')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">
                      Sütun Genişliği
                  </label>
                  <div class="col-7">
                    @if ($errors->has('props_sum_colvalue'))
                    <div id="props_sum_colvalue-error" class="form-control-feedback">{{ $errors->first('props_sum_colvalue') }}</div>
                    @endif
                    <select class="form-control m-select2" id="props_sum_colvalue" name="props_sum_colvalue"
                    @if (Request::segment(4) == 'delete')
                    disabled="disabled" 
                    @endif
                    >
                    @foreach (config('webshell.props.menu_default_sum_props.props_sum_colvalue') as $key => $sec)
                    <option value="{{ $sec['value'] }}"
                    @if (!empty(json_decode($top_has_sub->props)->props_sum_colvalue))
                    @if (json_decode($top_has_sub->props)->props_sum_colvalue == $sec['value']) 
                    {{ 'selected' }} 
                    @endif
                    @endif
                    > {{ $sec['name'] }} </option>
                    @endforeach
                </select>
            </div>
        </div>

        @endif

    </div>

    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-7">
                    @if (Request::segment(2) == 'add' || Request::segment(2) == 'edit')
                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                        Kaydet
                    </button>
                    &nbsp;&nbsp;
                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                        Vazgeç
                    </a>
                    @elseif (Request::segment(2) == 'delete')
                    <div class="alert alert-danger" role="alert">
                        <strong> Siliyorsunuz... </strong>
                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                    </div>
                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                        Kaydı Sil
                    </button>
                    &nbsp;&nbsp;
                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                        Vazgeç
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

</form>

</div>
</div>
@endsection

@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#topmenu').select2({
            placeholder: "Seçiniz..."
        });

        $('#type').select2({
            placeholder: "Seçiniz..."
        });

        $('#props_sum_type').select2({
            placeholder: "Seçiniz..."
        });

        $('#props_sum_count').select2({
            placeholder: "Seçiniz..."
        });
        $('#props_sum_colvalue').select2({
            placeholder: "Seçiniz..."
        });



        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    name: {
                        required: true
                    },
                    @if (Request::segment(2) == 'edit')
                    slug: {
                        required: true
                    },
                    @endif
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>
@endsection
