@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Sayfa Görseli ({{ $menu->variableLang($langs->first()->code)->name }})
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		
		<div class="m-portlet m-portlet--mobile">
			
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Sayfa Görseli ({{ $menu->variableLang($langs->first()->code)->name }})
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
                    
				</div>
			</div>

            <div class="m-portlet__body">
                
                <input type="hidden" name="menu_id" id="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                @foreach ($langs as $lang)
                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                [{{$lang->code}}] Sayfa Görseli Yükle 
                            </h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <div class="col-12" id="imgContainer{{$lang->code}}">
                            @if (!is_null($menu->variableLang($lang->code)->stvalue))
                                <img class="img-fluid" src="{{ url('upload/xlarge/'.$menu->variableLang($lang->code)->stvalue) }}" />
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <div class="col-12">
                            <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzoneStimageFileUpload{{$lang->code}}">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach

            </div>

            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-7">
                            @if (Request::segment(4) == 'view')
                                <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Geri
                                </a>
                            @elseif (Request::segment(4) == 'add' || Request::segment(4) == 'edit')
                                <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                    Kaydet
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            @elseif (Request::segment(4) == 'delete')
                                <div class="alert alert-danger" role="alert">
                                    <strong> Siliyorsunuz... </strong>
                                    Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                </div>
                                <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                    Kaydı Sil
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
		</div>
	</div>
@endsection


@section('inline-scripts')

@php
    $uploadConfig = config('webshell.upload.imageType.stimage');
@endphp

<script type="text/javascript">

    @foreach ($langs as $lang)
        Dropzone.options.dropzoneStimageFileUpload{{$lang->code}} = {
            headers: { 'X-CSRF-TOKEN': $('#token').val() },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: {{ $uploadConfig['maxFiles'] }},
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                done(); 
            },
            init: function () {
                //console.log('dropzone init');
            },
            sending: function(file, xhr, formData){
                formData.append('uploadType', '{{ $uploadConfig['uploadType'] }}');
                formData.append('menuid', $('#menu_id').val());
                formData.append('lang_code', '{{$lang->code}}');
            },
            success: function(file, xhr, event){
                var data = jQuery.parseJSON(xhr);
                $("#imgContainer{{$lang->code}}").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                this.removeFile(file);
            },
            transformFile: function(file, done) { 
                //console.log('transformFile');
                //console.log(file);
                var myDropZone = this;
                // Create the image editor overlay
                var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';
                document.body.appendChild(editor);
                // Create confirm button at the top left of the viewport
                var buttonConfirm = document.createElement('button');
                buttonConfirm.style.position = 'absolute';
                buttonConfirm.style.left = '10px';
                buttonConfirm.style.top = '10px';
                buttonConfirm.style.zIndex = 9999;
                buttonConfirm.textContent = 'Onayla';
                editor.appendChild(buttonConfirm);
                buttonConfirm.addEventListener('click', function() {
                    // Get the canvas with image data from Cropper.js
                    var canvas = cropper.getCroppedCanvas();
                    // Turn the canvas into a Blob (file object without a name)
                    canvas.toBlob(function(blob) {
                        // Create a new Dropzone file thumbnail

                        myDropZone.createThumbnail(
                            blob,
                            myDropZone.options.thumbnailWidth,
                            myDropZone.options.thumbnailHeight,
                            myDropZone.options.thumbnailMethod,
                            false, 
                            function(dataURL) {
                                // Update the Dropzone file thumbnail
                                myDropZone.emit('thumbnail', file, dataURL);
                                // Return the file to Dropzone
                                done(blob);
                            });
                        });
                    // Remove the editor from the view
                    document.body.removeChild(editor);
                });

                var buttonCancel = document.createElement('button');
                buttonCancel.style.position = 'absolute';
                buttonCancel.style.right = '10px';
                buttonCancel.style.top = '10px';
                buttonCancel.style.zIndex = 9999;
                buttonCancel.textContent = 'İptal';
                editor.appendChild(buttonCancel);

                buttonCancel.addEventListener('click', function() {
                    editor.remove();
                });

                var selectSize = document.createElement('select');
                selectSize.setAttribute("id", "selectSize");
                selectSize.style.position = 'absolute';
                selectSize.style.left = '40%';
                selectSize.style.top = '10px';
                selectSize.style.width = '20%';
                selectSize.style.zIndex = 9999;

                @foreach ($uploadConfig['type'] as $key => $value)
                    var opt = document.createElement('option');
                    opt.setAttribute("label", "{{ $value['title'] }}");
                    opt.setAttribute("value", {{ $value['aspectRatio'] }});
                    selectSize.appendChild(opt);
                @endforeach

                editor.appendChild(selectSize);

                selectSize.addEventListener('change', function() {
                    //console.log(this.value);
                    cropper.setAspectRatio(this.value);
                });

                // Create an image node for Cropper.js
                var image = new Image();
                image.src = URL.createObjectURL(file);
                editor.appendChild(image);

                // Create Cropper.js
                var cropper = new Cropper(image, { aspectRatio: {{ $uploadConfig['type']['default']['aspectRatio'] }}, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1});
                
            }
        };
    @endforeach

</script>
@endsection
