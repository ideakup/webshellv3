@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('language') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Dil Ayarlar
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		
		<div class="m-portlet m-portlet--mobile">
			
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Dil Ayarları
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
				</div>
			</div>
            @if ($languages->count() > 1)
    			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('language_save') }}" id="menuForm">
                    {{ csrf_field() }}
                    <div class="m-portlet__body">
                        
                        @foreach ($languages as $language)
                            
                            <div class="m-form__group form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ $language->name }}
                                </label>
                                <div class="col-3">
                                    <span class="m-switch">
                                        <label>
                                            <input type="checkbox" @if ($language->status == 'active' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif  id="{{ $language->code }}" name="{{ $language->code }}" value="active"/>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>

                        @endforeach

                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-7">
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endif
            
		</div>
	</div>
@endsection


@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function(){

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            /*
            form.validate({
                rules: {
                    name: {
                        required: true
                    },
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }
            */
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>
@endsection
