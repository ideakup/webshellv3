<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow">
            <li class="m-menu__item {{ (Request::segment(1) === 'menu') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("menu/list") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fa fa-sitemap"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Menü
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item {{ (Request::segment(1) === 'calendar') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("calendar/list") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fa fa-calendar-alt"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Takvim
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item {{ (Request::segment(1) === 'photogallery') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("photogallery") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fas fa-image"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Foto Galeri
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item {{ (Request::segment(1) === 'tag') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("tag") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fas fa-tags"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Etiket
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item {{ (Request::segment(1) === 'category') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("category") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fas fa-bezier-curve"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Kategori
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item {{ (Request::segment(1) === 'form') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("form/list") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fa fa-list-alt"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Formlar
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            @if (App\Language::count() > 1)
                <li class="m-menu__item {{ (Request::segment(1) === 'language') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ url("language") }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fas fa-flag"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Dil Ayarları
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif
            <li class="m-menu__item {{ (Request::segment(1) === 'sitesettings') ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ url("sitesettings") }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fas fa-cogs"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Site Ayarları
                            </span>
                        </span>
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>