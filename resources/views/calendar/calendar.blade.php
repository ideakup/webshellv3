@extends('layouts.webshell')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Takvim
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Takvim Görüntüsü
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Takvim Görüntüsü
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
            </div>
            <div class="m-portlet__body">
                <style type="text/css">
                    .fc-event-container .tooltip-inner {
                        background-color: #c5ccd5;
                        color: #000;
                        text-align: unset;
                    }
                    .fc-event-container .tooltip-inner li {
                        list-style: none;
                    }
                    .fc-event-container .tooltip-arrow{
                        border-top-color: #c5ccd5 !important;
                    }
                </style>
                <div id="calendar"></div>
            </div>
        </div>
    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#calendar').fullCalendar({
                themeSystem: 'bootstrap3',
                locale: 'tr',
                displayEventTime: false,
                displayEventEnd: false,
                url: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,listMonth'
                },
                events: [
                    @foreach ($calendar as $element)
                    @php
                        if($element->owner == 'muze'){
                            $bgcolor = '#D40B2F';
                        }else if($element->owner == 'vakif'){
                            $bgcolor = '#FC8425';
                        }else if($element->owner == 'kutuphane'){
                            $bgcolor = '#7E2335';
                        }else if($element->owner == 'dukkan'){
                            $bgcolor = '#FED731';
                        }else if($element->owner == 'fillibahce'){
                            $bgcolor = '#64AC49';
                        }
                    @endphp
                    {
                        id: '{{ $element->id }}',
                        title: '{!! str_replace("'", "\'", str_replace('"', '\"', $element->variable->title)) !!}',
                        start: '{{ $element->start }}',
                        end: '{{ $element->end }}',
                        editable: '{{-- $element->editable --}}',
                        color: '{{ $bgcolor }}',
                        description: 'Ücret: {{ is_null($element->price)?'Ücretsiz':$element->price.' ₺' }}<br>Kapasite: {{ is_null($element->capacity)?'Sınırsız':$element->capacity }}<br>'+'{!!$element->variable->address!!}'
                    },
                    @endforeach
                ],
                eventRender: function(event, element) {
                    element.popover({
                        container: 'body',
                        html: true,
                        placement: 'auto',
                        title: '<strong>'+event.title+'</strong>',
                        content: event.description,
                        trigger: 'hover',
                        //container: '.popover-content',
                        //template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content">asdas</div></div>'
                    });
                }
            });
        });
    </script>
@endsection
