@php

    $_order_visible = false;
    $_order_value = '';
    $_order_disabled = '';
    // $_order_visible
    if (Request::segment(4) == 'add' || (Request::segment(4) == 'edit' && is_null(Request::segment(6)))){
        $_order_visible = true;
    }
    
    // $_order_value
    if (Request::segment(4) != 'add' && empty(old('order'))) {
        $_order_value =  $ths->order;
    } else {
        $_order_value = old('order');
    }

    // $_order_disabled
    if (Request::segment(4) == 'delete'){
        $_order_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_order_visible)
    <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Sıralama
        </label>
        <div class="col-7">
            <input class="form-control m-input" type="number" min="1" max="1000000" id="order" name="order" value="{{ $_order_value }}" {!! $_order_disabled !!} required>
            
            @if ($errors->has('order'))
                <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
            @endif
        </div>
    </div>
@endif