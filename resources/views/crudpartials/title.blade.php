<!-- OK -->
@php
    $_title_visible = true;
    $_title_value = '';
    $_title_disabled = '';

    // $_title_visible

    // $_title_value
    if (Request::segment(4) != 'add' && empty(old('title'))) {
        if(is_null($content->variableLang(Request::segment(6)))){
            $_title_value =  $content->variableLang($langs->first()->code)->title;
        } else {
            $_title_value = $content->variableLang(Request::segment(6))->title;
        }
    } else {
        $_title_value = old('title');
    }

    // $_title_disabled
    if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6))))){
        $_title_disabled = ' disabled="disabled" ';
    }

@endphp

<div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Başlık
    </label>
    <div class="col-7">
        <input class="form-control m-input" type="text" id="title" name="title" value="{{ $_title_value }}" {!! $_title_disabled !!} required autofocus>
        
        @if ($errors->has('title'))
            <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
        @endif

    </div>
</div>