@if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))
    @if ($content->type == 'text')
        
        <script type="text/javascript">
            /*
            tinymce.init({
                selector: 'textarea#content',
                height: 500,
                plugins: 'table wordcount',
                content_style: '.left { text-align: left; } ' +
                    'img.left { float: left; } ' +
                    'table.left { float: left; } ' +
                    '.right { text-align: right; } ' +
                    'img.right { float: right; } ' +
                    'table.right { float: right; } ' +
                    '.center { text-align: center; } ' +
                    'img.center { display: block; margin: 0 auto; } ' +
                    'table.center { display: block; margin: 0 auto; } ' +
                    '.full { text-align: justify; } ' +
                    'img.full { display: block; margin: 0 auto; } ' +
                    'table.full { display: block; margin: 0 auto; } ' +
                    '.bold { font-weight: bold; } ' +
                    '.italic { font-style: italic; } ' +
                    '.underline { text-decoration: underline; } ' +
                    '.example1 {} ' +
                    '.tablerow1 { background-color: #D3D3D3; }',
                formats: {
                    alignleft: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'left' },
                    aligncenter: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'center' },
                    alignright: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'right' },
                    alignfull: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'full' },
                    bold: { inline: 'span', classes: 'bold' },
                    italic: { inline: 'span', classes: 'italic' },
                    underline: { inline: 'span', classes: 'underline', exact: true },
                    strikethrough: { inline: 'del' },
                    customformat: { inline: 'span', styles: { color: '#00ff00', fontSize: '20px' }, attributes: { title: 'My custom format'} , classes: 'example1'}
                },
                style_formats: [
                    { title: 'Custom format', format: 'customformat' },
                    { title: 'Align left', format: 'alignleft' },
                    { title: 'Align center', format: 'aligncenter' },
                    { title: 'Align right', format: 'alignright' },
                    { title: 'Align full', format: 'alignfull' },
                    { title: 'Bold text', inline: 'strong' },
                    { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
                    { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
                    { title: 'Badge', inline: 'span', styles: { display: 'inline-block', border: '1px solid #2276d2', 'border-radius': '5px', padding: '2px 5px', margin: '0 2px', color: '#2276d2' } },
                    { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' },
                    { title: 'Image formats' },
                    { title: 'Image Left', selector: 'img', styles: { 'float': 'left', 'margin': '0 10px 0 10px' } },
                    { title: 'Image Right', selector: 'img', styles: { 'float': 'right', 'margin': '0 0 10px 10px' } },
                ]
            });
            */
            
            tinymce.init({
                selector: 'textarea#content',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste template',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'forMoreCustomButton | template | undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,

                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
                },
                extended_valid_elements: "formore",
                custom_elements: "formore",
                setup: function (editor) {
                    editor.ui.registry.addButton('forMoreCustomButton', {
                        icon: 'flip-vertically',
                        onAction: function (_) {
                            editor.insertContent('<formore></formore>');
                        }
                    });
                },
                content_css: "/css/tinymce_inline.css",
                templates: [
                    {title: 'Yorum Paneli', description: 'Yorum şablonunu kullanmak için seçiniz...', content: '<div class="yorum-cerceve"><p class="tinymce-yorum-baslik">Adı Soyadı</p><p class="tinymce-yorum-tarih">01.01.2020</p><p class="tinymce-yorum-metin">Yorum metnini buraya ekleyiniz</p></div>'}
                ],

            });
            
            /*
                Dropzone.options.dropzoneBgFileUpload = {
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        formData.append('uploadType', 'contentbgimage');
                        formData.append('contentid', {{ $content->id }});
                        formData.append('lang_code', '{{ Request::segment(6) }}');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#bgImgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                        this.removeFile(file);
                    }
                };
            */

        </script>

    @endif
@endif

