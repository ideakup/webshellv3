@php

    $_code_visible = false;
    $_code_value = '';
    $_code_disabled = '';
    
    // $_code_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
    	if ($content->type == 'code'){
    		$_code_visible = true;
    	}
    }
    
    // $_code_value
    if (Request::segment(4) != 'add' && empty(old('code'))) {
		if (is_null($content->variableLang(Request::segment(6)))) {
			$_code_value = $content->variableLang($langs->first()->code)->content;
		} else {
			$_code_value = $content->variableLang(Request::segment(6))->content;
		}   
	} else {
		$_code_value = old('code');
	}

    // $_code_disabled
    if (Request::segment(4) == 'delete'){
        $_code_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_code_visible)

	@if ($content->type == 'code')

		<div class="form-group m-form__group" style="padding-bottom: 0;">
		    <label>Code</label>
		</div>

		<div class="form-group m-form__group @if ($errors->has('code')) has-danger @endif">
		    <textarea class="form-control m-input" id="code" name="code" rows="15">{!! $_code_value !!}</textarea>

		    @if ($errors->has('code'))
		        <div id="code-error" class="form-control-feedback">{{ $errors->first('code') }}</div>
		    @endif
		</div>

	@endif

@endif