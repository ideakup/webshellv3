@php

    $_dropzone_visible = false;
    $_dropzone_type = '';
    $_dropzone_value = '';
    $_dropzone_disabled = '';
    
    // $_dropzone_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
        if ($content->type == 'photo') {
            $_dropzone_visible = true;
            $_dropzone_type = 'photo';
        }elseif($content->type == 'photogallery'){
            $_dropzone_visible = true;
            $_dropzone_type = 'photogallery';
        }elseif($content->type == 'slide'){
            $_dropzone_visible = true;
            $_dropzone_type = 'slide';
        }
    }elseif(Request::segment(4) == 'mapmarker' && Request::segment(6) == 'edit' && !is_null(Request::segment(8))){
        $_dropzone_visible = true;
        $_dropzone_type = 'mapturkey';
    }

    
    // $_dropzone_value
    if (Request::segment(4) != 'add' && empty(old('order'))) {
        $_dropzone_value =  $content->order;
    } else {
        $_dropzone_value = old('order');
    }


    // $_dropzone_disabled
    if (Request::segment(4) == 'delete'){
        $_dropzone_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_dropzone_visible)

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">

                    @if ($_dropzone_type == 'photo')
                        <h3 class="m-portlet__head-text"> Fotoğraf Yükle </h3>
                    @elseif($_dropzone_type == 'photogallery')
                        <h3 class="m-portlet__head-text"> Fotoğraf Galerisi Yükle </h3>
                    @elseif($_dropzone_type == 'slide')
                        <h3 class="m-portlet__head-text"> Slide Yükle </h3>
                    @elseif($_dropzone_type == 'mapturkey')
                        <h3 class="m-portlet__head-text"> Fotoğraf Yükle </h3>
                    @endif

                </div>
            </div>
        </div>

        <div class="m-portlet__body">

            <div class="form-group m-form__group row">

                <div class="col-12">
                    <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzoneFileUpload">
                        <div class="m-dropzone__msg dz-message needsclick">
                            <h3 class="m-dropzone__msg-title">
                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın...
                            </h3>
                            <span class="m-dropzone__msg-desc">
                                Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                            </span>
                        </div>
                    </div>
                </div>

            </div>
                
            @if ($_dropzone_type == 'photo')
                
                <div class="form-group m-form__group row">
                    
                    @php $isImgExist = false; @endphp
                    @if (!is_null($content->variableLang(Request::segment(6))) && !empty($content->variableLang(Request::segment(6))->content))
                        @php $isImgExist = true; @endphp
                    @endif
                        
                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">

                                        <li class="m-portlet__nav-item" aria-expanded="true">
                                            <a href="#" id="cropPhotoButton" @if($isImgExist) onclick="photoGalleryThumbnailCrop('{{ url('upload/xlarge/'.$content->variableLang(Request::segment(6))->content) }}', {{$content->variableLang(Request::segment(6))->id}}, '{{ $_dropzone_type }}'); return false;" @endif class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                <i class="fa fa-crop"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="imgContainer">
                                        @if($isImgExist)
                                            <img class="img-fluid" src="{{ url('upload/xlarge/'.$content->variableLang(Request::segment(6))->content) }}" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body" style="padding-bottom: 0;">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="thumbnailContainer">

                                        <img class="img-fluid" id="thumbnail-{{ $content->variableLang(Request::segment(6))->id }}" @if($isImgExist) src="{{ url('upload/thumbnail/'.$content->variableLang(Request::segment(6))->content) }}" @endif />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            @elseif($_dropzone_type == 'photogallery')
                
                <div class="form-group m-form__group row" id="imgContainer">
                    @foreach ($content->photogallery->where('lang', Request::segment(6)) as $img)
                        <div class="col-4">
                            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                                <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            
                                            <li class="m-portlet__nav-item" aria-expanded="true">
                                                <a href="{{ url('/menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/'.$img->id.'/'.Request::segment(6)) }}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                                <a href="#" onclick="photoGalleryThumbnailCrop('{{ url('upload/org/'.$img->url) }}', {{$img->id}}, '{{ $_dropzone_type }}'); return false;" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                    <i class="fa fa-crop"></i>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="m-widget19">
                                        <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
                                            <img class="img-fluid" id="thumbnail-{{ $img->id }}" src="{{ url('upload/thumbnail/'.$img->url) }}" />
                                            <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">
                                                {{ $img->name }}
                                            </p>
                                            <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            @elseif($_dropzone_type == 'slide')
                
                <div class="form-group m-form__group row" id="imgContainer">
                    
                    @foreach ($content->slide as $img)
                        <div class="col-4">
                            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                                <div class="m-portlet__head m-portlet__head--fit">
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            <li class="m-portlet__nav-item" aria-expanded="true">
                                                <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/slide_edit/'.$img->id.'/'.Request::segment(6)) }}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="m-widget19">
                                        <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
                                            <img class="img-fluid" src="{{ url('upload/thumbnail/'.$img->image_url) }}" />
                                            <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">
                                                {{ $img->title }}
                                            </p>
                                            <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

            @elseif($_dropzone_type == 'mapturkey')

                <div class="form-group m-form__group row">

                    @php $isImgExist = false; @endphp
                    @if (!is_null($staff->variableLang(Request::segment(8))) && !empty($staff->variableLang(Request::segment(8))->photo_url))
                        @php $isImgExist = true; @endphp
                    @endif
                        
                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">

                                        <li class="m-portlet__nav-item" aria-expanded="true">
                                            <a href="#" id="cropPhotoButton" @if($isImgExist) onclick="photoGalleryThumbnailCrop('{{ url('upload/xlarge/'.$staff->variableLang(Request::segment(8))->photo_url) }}', {{$staff->variableLang(Request::segment(8))->id}}, '{{ $_dropzone_type }}'); return false;" @endif class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                <i class="fa fa-crop"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="imgContainer">
                                        @if($isImgExist)
                                            <img class="img-fluid" src="{{ url('upload/xlarge/'.$staff->variableLang(Request::segment(8))->photo_url) }}" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body" style="padding-bottom: 0;">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="thumbnailContainer">

                                        <img class="img-fluid" id="thumbnail-{{ $staff->variableLang(Request::segment(8))->id }}" @if($isImgExist) src="{{ url('upload/thumbnail/'.$staff->variableLang(Request::segment(8))->photo_url) }}" @endif />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            @endif
            
        </div>

    </div>

@endif