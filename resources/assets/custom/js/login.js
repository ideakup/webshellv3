//== Class Definition
var SnippetLogin = function() {

    var login = $('#m_login');

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
            <span></span>\
        </div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        mUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    var rememberCookie = function() {
        var remember;
        if (Cookies.get("remember_email") === undefined) {
            remember = false;
            $('#remember').attr('checked', false);
        } else {
            remember = true;
            $('#email').hide();
            $('#remember_label').hide();
            $('#email').val(Cookies.get("remember_email"));
            $('#password').focus();
            $('#remember').attr('checked', true);

            $.ajax({
                url: "getusername/" + Cookies.get("remember_email"),
            }).done(function(a) {
                console.log(a);
                if (a.avatar != null) {
                    $('#user_avatar').attr('src', a.avatar);
                } else {
                    $('#user_avatar').attr('src', '/images/avatars/default.jpg');
                }
                $('#user_name').append(a.name);
                $('#user_email').append(Cookies.get("remember_email"));
                $('#password').focus();
                $('#change_user_div').fadeIn("slow");
                $('#user_card_div').fadeIn("slow");
            });
        }

        $('#change_user').click(function() {
            $('#email').val('');
            $('#password').val('');
            $('#remember').attr('checked', false);

            $('#user_card_div').hide();
            $('#change_user_div').hide();
            $('#email').show();
            $('#remember_label').show();

            $('#email').focus();
        });
    }

    var setRememberCookie = function(form, status) {

        if (status == 'success' && $('#remember').is(':checked')) {
            Cookies.set("remember_email", $('#' + form.attr('id') + ' #email').val(), { expires: 5 });
        } else {
            Cookies.remove('remember_email');
        }
    }

    //== Private Functions
    var displaySignInForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        mUtil.animateClass(login.find('.m-login__signin')[0], 'flipInX animated');
    }

    var displayForgetPasswordForm = function() {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        mUtil.animateClass(login.find('.m-login__forget-password')[0], 'flipInX animated');
    }

    var handleFormSwitch = function() {
        $('#m_login_forget_password').click(function(e) {
            e.preventDefault();
            displayForgetPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function() {
        $('#m_login_signin_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            var inputs = $('#' + form.attr('id') + ' input');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            inputs.attr('readonly', true);

            form.ajaxSubmit({
                url: 'login',
                success: function(response, status, xhr, $form) {
                    setRememberCookie(form, status);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    inputs.attr('disabled', true);
                    showErrorMsg(form, 'success', 'Login Success...');
                    location.reload();
                },
                error: function(xhr, status, message, $form) {
                    setRememberCookie(form, status);
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                        inputs.attr('readonly', false);
                    }, 1000);
                }
            });

        });
    }

    var handleForgetPasswordFormSubmit = function() {
        $('#m_login_forget_password_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');
            
            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: 'password/email',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove 
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.m-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Şifre Sıfırlama adresi e-posta adresinize gönderilmiştir.');
                    }, 2000);
                }
            });


        });
    }

    //== Public Functions
    return {
        // public functions
        init: function() {
            rememberCookie();
            handleFormSwitch();
            handleSignInFormSubmit();
            handleForgetPasswordFormSubmit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function() {
    SnippetLogin.init();
});