<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Content extends Model
{
    protected $table = 'content';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\ContentVariable', 'content_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\ContentVariable', 'content_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\ContentVariable', 'content_id', 'id');
    }

    public function topContentThs()
    {
        return $this->hasMany('App\TopHasSub', 'sub_content_id', 'id')->orderBy('order', 'asc');
    }

    public function subContentThs()
    {
        return $this->hasMany('App\TopHasSub', 'top_content_id', 'id')->orderBy('order', 'asc');
    }






    public function menu()
    {
        return $this->belongsToMany('App\Menu', 'menu_has_content');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'content_has_tag');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'content_has_category');
    }

    /*
    public function sub_content()
    {
        return $this->hasMany('App\Content', 'top_content', 'id')->where('deleted', 'no');
    }

    public function top_content()
    {
        return $this->hasOne('App\Content', 'id', 'top_content')->where('deleted', 'no');
    }
    */
    
    public function photogallery()
    {
        return $this->hasMany('App\ContentPhotoGalleryVariable', 'content_id', 'id')->where('deleted', 'no')->orderBy('order', 'asc');
    }

    public function slide()
    {
        return $this->hasMany('App\ContentSlideVariable', 'content_id', 'id')->where('deleted', 'no');
    }
  
}
