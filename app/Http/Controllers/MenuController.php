<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\FormData;
use App\Form;

use App\District;
use App\DistrictVariable;
use App\Staff;
use App\StaffVariable;

use App\Country;
use App\CountryVariable;
use App\City;
use App\CityVariable;
use App\County;
use App\CountyVariable;

use App\MenuHasContent;
use App\TopHasSub;

use App\ContentHasTag;
use App\ContentHasCategory;
use App\ContentSlideVariable;

use App\ContentPhotoGalleryVariable;
use App\Slider;
use App\SliderVariable;

use App;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        //dump(FormData::select('source_id', 'source_type', 'form_id')->distinct()->get());

        $uniqForms = FormData::select('source_id', 'source_type', 'form_id')->distinct()->get();

        echo 'test';
    }

    /** MENÜ **/
    public function list()
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('menu.list', array('langs' => $langs));
    }

    public function add_edit($id = null, $lang = null)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $topmenus = Menu::where('deleted', 'no')->get();
        if(is_null($id)){
            $menu = new Menu();
        }else{
            $menu = Menu::find($id);
        }
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
    }

    public function delete($id = null)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
    }

    public function save(Request $request)
    {
            //dd($request->input());
        $text = "";
        if ($request->crud == 'add') {
            $rules = array(
                'name' => 'required|max:255',
                'order' => 'numeric|min:1|max:100000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
           }

           $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

           $menu = new Menu();
           if ($request->topmenu != 'null') { 
            $menu->top_id = $request->topmenu; 
        }
        $menu->type = $request->type;
        $menu->description = $request->description;
        $menu->order = $request->order;
        $menu->position = $request->position.'';
        $menu->status = ($request->status == 'active') ? 'active' : 'passive';
        $menu->save();

        foreach ($languages  as $language) {
            $menuVariable = new MenuVariable();
            $menuVariable->menu_id = $menu->id;
            $menuVariable->lang_code = $language->code;
            $menuVariable->name = $request->name;

                    $menuVariable->slug = str_slug($request->name.'-'.$language->code, '-'); //SLU-GGG
                    $slugCount = MenuVariable::where('slug',$menuVariable->slug)->count();
                        if($slugCount!=0){
                             $random = str_random(3);
                             $menuVariable->slug =$menuVariable->slug.$random;
                        }

                    $menuVariable->menutitle = $request->menutitle;
                    $menuVariable->title = $request->title;

                    if ($menu->type == 'link') {
                        $menuVariable->stvalue = json_encode(['link' => '#', 'target' => 'self']);
                    }

                    $menuVariable->save();
                }

                $text = 'Başarıyla Eklendi...';

            }
            else if($request->crud == 'edit'){

                if(is_null($request->top_menu_id) && is_null($request->top_content_id)){

                    if (is_null($request->lang)) {
                        $rules = array(
                            'order' => 'numeric|min:1|max:100000'
                        );
                        $validator = Validator::make(Input::all(), $rules);
                        if ($validator->fails()) {
                           return \Redirect::back()->withErrors($validator)->withInput();
                       }
                       $menu = Menu::find($request->menu_id);

                       if ($request->topmenu == 'null') { 
                        $menu->top_id = null; 
                    }else{
                        $menu->top_id = $request->topmenu; 
                    }
                    $menu->description = $request->description;
                    $menu->order = $request->order;
                    $menu->position = $request->position.'';
                    $menu->status = ($request->status == 'active') ? 'active' : 'passive';
                    if ($menu->type == 'link') {
                        $menu->headertheme = 'light';
                        $menu->slidertype = 'no';
                        $menu->breadcrumbvisible = 'no';
                        $menu->asidevisible = 'no';
                        $menu->dropdowntype = 'normal';
                        $menu->listtype = null;
                    }else{
                        $menu->headertheme = $request->headertheme.'';
                        $menu->slidertype = $request->slidertype.'';
                        $menu->breadcrumbvisible = $request->breadcrumbvisible.'';
                        $menu->asidevisible = $request->asidevisible.'';
                        $menu->dropdowntype = $request->dropdowntype.'';
                        if ( starts_with($menu->type, 'list') || $menu->type == 'photogallery') {
                            $menu->listtype = $request->listtype.'';
                        }else{
                            $menu->listtype = null;
                        }
                    }
                    $menu->save();
                }else{

                        //dump($request->input());

                    $slugCount = MenuVariable::where('slug', str_slug($request->slug, '-'))->count();
                    $slugControl = true;
                    if($slugCount > 0){
                        $slugMenuVars = MenuVariable::where('slug', str_slug($request->slug, '-'))->get();
                        foreach ($slugMenuVars as $slugMenuVar) {
                            if($slugMenuVar->menu_id == $request->menu_id && $slugMenuVar->lang_code == $request->lang){
                            }else{
                                $slugControl = false;
                            }
                                //dump($slugMenuVar);
                        }
                    }

                        //dump($slugControl);
                        //dd($slugCount);

                    $rules = array(
                        'name' => 'required|max:255'
                    );

                    $validator = Validator::make(Input::all(), $rules);

                    if (!$slugControl) {
                        $validator->after(function ($validator) {
                            $validator->errors()->add('slug', 'Bu alan benzersiz olmalı.');
                        });
                    }

                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }

                   $menuVariable = Menu::find($request->menu_id)->variableLang($request->lang);
                   if (!isset($menuVariable)) {
                    $menuVariable = new MenuVariable();
                    $menuVariable->menu_id = $request->menu_id;
                    $menuVariable->lang_code = $request->lang;
                }
                $menuVariable->name = $request->name;
                        $menuVariable->slug = str_slug($request->slug, '-'); //SLUGGG
                        $menuVariable->menutitle = $request->menutitle;
                        $menuVariable->title = $request->title;
                        if ($menuVariable->menu->type == 'link') {
                            $menuVariable->stvalue = json_encode(['link' => $request->stvalue_link, 'target' => $request->stvalue_target]);
                        }
                        $menuVariable->save();
                    }

                }else if(!is_null($request->top_menu_id)){
                    //dd('top_menu_id');
                    $top_has_sub = TopHasSub::where('top_menu_id', $request->top_menu_id)->where('sub_menu_id', $request->menu_id)->first();

                    $_props = array();
                    $_props['props_sum_type'] = $request->props_sum_type;
                    $_props['props_sum_count'] = $request->props_sum_count;
                    $_props['props_sum_colvalue'] = $request->props_sum_colvalue;

                    $top_has_sub->props = json_encode($_props);
                    $top_has_sub->order = $request->order;
                    $top_has_sub->save();

                }else if(!is_null($request->top_content_id)){

                    //dd($request->input());
                    $top_has_sub = TopHasSub::where('top_content_id', $request->top_content_id)->where('sub_menu_id', $request->menu_id)->first();

                    $_props = array();
                    $_props['props_sum_type'] = $request->props_sum_type;
                    $_props['props_sum_count'] = $request->props_sum_count;
                   // $_props['props_sum_colvalue'] = $request->props_sum_colvalue;

                    $top_has_sub->props = json_encode($_props);
                    $top_has_sub->order = $request->order;
                    $top_has_sub->save();
                }

                $text = 'Başarıyla Düzenlendi...';

            }else if($request->crud == 'delete'){
                //dd($request->input());
                if(empty($request->top_menu_id)){
                    $menu = Menu::find($request->menu_id);
                    $menu->deleted = 'yes';
                    $menu->status = 'passive';
                    $menu->save();
                    foreach ($menu->variables as $menuVariable) {
                        $menuVariable->slug = null;
                        $menuVariable->save();
                    }
                }else{
                    //dd('aa');
                    TopHasSub::where('top_menu_id', $request->top_menu_id)->where('sub_menu_id', $request->menu_id)->delete();
                }
                
                $text = 'Başarıyla Silindi...';

            }
            return redirect('menu/list')->with('message', array('text' => $text, 'status' => 'success'));
        }
        /** MENÜ **/

        /** CONTENT **/
        public function content($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);

            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }

            return view('menu.content', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
        }

        public function content_add($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            
            $menu = Menu::find($id);
            $ths = null;
            if(is_null($cid)){
                $content = null;
            }else{
                $content = Content::find($cid);
            }

            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_edit($id, $cid, $lang = null)
        {

            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $menu = Menu::find($id);

            $content = Content::find($cid);
            if(!is_null($lang) && is_null($content->variableLang($lang))){
                $contentVariable = new ContentVariable();
                $contentVariable->content_id = $content->id;
                $contentVariable->lang_code = $lang;
                $contentVariable->title = '';
                $contentVariable->save();
            }
            
            if(!empty(Input::get('tcid'))){
                $ths = TopHasSub::where('top_menu_id', $id)->where('top_content_id', Input::get('tcid'))->where('sub_content_id', $cid)->first();
            }else{
                $ths = TopHasSub::where('top_menu_id', $id)->where('sub_content_id', $cid)->first();
            }

            //dd(array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_delete($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $ths = TopHasSub::where('top_menu_id', $id)->where('sub_content_id', $cid)->first();
            $content = Content::find($cid);
            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if($request->crud == 'add'){

                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
               $menu = Menu::find($request->menu_id);

               $content = new Content();
               $content->type = $request->type.'';
               //$content->order = $request->order;
               $content->status = ($request->status == 'active') ? 'active' : 'passive';
               $content->save();

               foreach ($languages as $language) {

                $contentVariable = new ContentVariable();
                $contentVariable->content_id = $content->id;
                $contentVariable->lang_code = $language->code;
                $contentVariable->title = $request->title;

                if ($content->type == 'link') {
                    $_content = array();
                    $_content['content_url'] = '#';
                    $_content['content_target'] = 'internal' ;
                    $contentVariable->content = json_encode($_content);
                }

                if ($content->type == 'text' || $content->type == 'rssfeed' || $content->type == 'mapturkey' || $content->type == 'form' || $content->type == 'link' || $content->type == 'photo'  || $content->type == 'slide'|| $content->type == 'photogallery' || $content->type == 'code' || $content->type == 'group' || $content->type == 'seperator') {
                    $_props = array();
                    $_props['props_section'] = 'container';
                    $_props['props_colortheme'] = 'light';
                    $_props['props_colvalue'] = 'col-lg-12';
                        if($menu->type=="list-icon"){
                        $_props['props_icon'] = "fa";// ilk icon eklemede patlamaması için
                        }
                        if($menu->type=="list-ekip"){
                        $_props['props_stafftype'] = " ";// ilk çalışan tipi eklemede patlamaması için
                        $_props['props_title'] = " ";// ilk çalışan ünvanı eklemede patlamaması için

                        }
                    if ($content->type == 'seperator') {
                        $_props['props_type'] = 'normal';
                    }

                    if ($content->type == 'photo') {
                        $_props['props_type'] = 'responsive';
                        $_props['photo_url'] = '#';//list photo için patlama sorunu düzeltildi

                    }

                    if ($content->type == 'photogallery') {
                        $_props['props_type'] = 'col-3x';
                    }

                    $contentVariable->props = json_encode($_props);
                }

                if (is_null($request->menu_id)) {
                    $contentVariable->save();
                }else{




                    if ($content->type == 'mapturkey') {
                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug', $contentVariable)->count();
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->$contentVariable.$random;
                            }

                        }

                        if ($content->type == 'group')  {


                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug',$contentVariable->slug)->count();
                           // dd($slugCount);
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->slug=$contentVariable->slug.$random;
                            }
                        }


                    }

                    $contentVariable->save();

                }


                if (!is_null($request->menu_id) && !is_null($request->content_id)) {

                    $top_has_sub = new TopHasSub();
                    $top_has_sub->top_menu_id = $request->menu_id;
                    $top_has_sub->top_content_id = $request->content_id;
                    $top_has_sub->sub_content_id = $content->id;
                    $top_has_sub->order = $request->order;

                    $_props = array();
                    if ($content->type == 'form') {
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                    }
                    $top_has_sub->props = json_encode($_props);

                    $top_has_sub->save();

                } elseif (!is_null($request->menu_id) && is_null($request->content_id)) {

                    $top_has_sub = new TopHasSub();
                    $top_has_sub->top_menu_id = $request->menu_id;
                    $top_has_sub->sub_content_id = $content->id;
                    $top_has_sub->order = $request->order;

                    $_props = array();
                    if ($content->type == 'form') {
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                    }
                    $top_has_sub->props = json_encode($_props);

                    $top_has_sub->save();

                } elseif (is_null($request->menu_id) && !is_null($request->content_id)) {

                    dd("null : !null");

                } elseif (is_null($request->menu_id) && is_null($request->content_id)) {

                    dd("null : null");

                }


                $text = 'Başarıyla Eklendi...';
                
                if ($request->menu_id == 0 && is_null($request->content_id)) {
                    return redirect('form/list/')->with('message', array('text' => $text, 'status' => 'success'));
                }else{

                    if(!is_null($request->content_id)){
                        return redirect('menu/content/'.$request->menu_id.'/subcontent/'.$request->content_id)->with('message', array('text' => $text, 'status' => 'success')); 
                    }else{
                        return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success')); 
                    }

                }

            }else if($request->crud == 'edit'){

                if (is_null($request->lang)) {

                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }
                   $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

                   $menu = Menu::find($request->menu_id);
                   if(empty($request->top_content_id)){
                    $ths = TopHasSub::where('top_menu_id', $request->menu_id)->where('sub_content_id', $request->content_id)->first();
                }else{
                    $ths = TopHasSub::where('top_menu_id', $request->menu_id)->where('top_content_id', $request->top_content_id)->where('sub_content_id', $request->content_id)->first();
                }
                $content = Content::find($request->content_id);


                $_props = array();
                if ($content->type == 'form') {

                    $_props['props_eposta'] = $request->props_eposta;
                    $_props['props_buttonname'] = $request->props_buttonname;
                    $_props['props_comment_status'] = $request->props_comment_status;

                    if($request->props_comment_status == 'active'){
                        $_props['props_visible_elemenets'] = $request->formelements;
                    }

                }
                $ths->props = json_encode($_props);

                $ths->order = $request->order;
                $ths->save();

                $content->status = ($request->status == 'active') ? 'active' : 'passive';
                $content->save();

                if ($content->type == 'text') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }
                    
                }

                if ($content->type == 'slide') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }
                    
                }

                if ($content->type == 'rssfeed') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'mapturkey') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'form') {

                    foreach ($languages as $language) {
                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';
                            $contentVariable->save();
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;

                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'seperator') {

                    foreach ($languages as $language) {
                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;

                        $_props['props_type'] = $request->props_type;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'link') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'photo') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $_props['props_type'] = $request->props_type;
                        $_props['photo_url'] = $request->photo_url;

                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }
                    
                }

                if ($content->type == 'photogallery') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $_props['props_type'] = $request->props_type;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();

                    }
                    
                }

                if ($content->type == 'code') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        $contentVariable->props = json_encode($_props);

                        $contentVariable->save();
                    }

                }

                if ($content->type == 'group') {
                    foreach ($languages as $language) {

                        $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                        if(is_null($contentVariable)){
                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $request->content_id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = '';                                
                        }

                        $_props = array();
                        $_props['props_section'] = $request->props_section;
                        $_props['props_colortheme'] = $request->props_colortheme;
                        $_props['props_colvalue'] = $request->props_colvalue;
                        if($menu->type=="list-icon"){
                              $_props['props_icon'] = $request->props_icon;
                        }
                        if($menu->type=="list-ekip"){
                              $_props['props_stafftype'] =$request->props_stafftype;
                              $_props['props_title'] = $request->props_title;
                        }

                        if(!empty(config('webshell.props.menu.'.$menu->type))){
                            foreach (config('webshell.props.menu.'.$menu->type) as $key => $field){
                                $_props['props_'.$key] = $request['props_'.$key];
                            }
                        }

                        $contentVariable->props = json_encode($_props);
                        $contentVariable->save();
                    }
                    
                }

                $tagItems = ContentHasTag::where('content_id', $request->content_id)->delete();
                if (!is_null($request->tag)) {
                    foreach ($request->tag as $tt) {
                        $tagItem = new ContentHasTag();
                        $tagItem->content_id = $request->content_id;
                        $tagItem->tag_id = $tt;
                        $tagItem->save();
                    }
                }

                $categoryItems = ContentHasCategory::where('content_id', $request->content_id)->delete();
                if (!is_null($request->category)) {
                    foreach ($request->category as $cc) {
                        $categoryItem = new ContentHasCategory();
                        $categoryItem->content_id = $request->content_id;
                        $categoryItem->category_id = $cc;
                        $categoryItem->save();
                    }
                }

                $text = 'Başarıyla Düzenlendi...';

            }else{

                    //dump($request->input());
                $slugCount = ContentVariable::where('slug', '!=', '')->whereNotNull('slug')->where('slug', str_slug($request->slug, '-'))->count();
                $slugControl = true;
                if($slugCount > 0){
                    $slugContentVars = ContentVariable::where('slug', '!=', '')->whereNotNull('slug')->where('slug', str_slug($request->slug, '-'))->get();
                    foreach ($slugContentVars as $slugContentVar) {
                        if($slugContentVar->content_id == $request->content_id && $slugContentVar->lang_code == $request->lang){
                        }else{
                            $slugControl = false;
                        }
                            //dump($slugContentVar);
                    }
                }
                    //dump($slugControl);
                    //dd($slugCount);

                $rules = array(
                    'title' => 'required|max:255'
                );
                $validator = Validator::make(Input::all(), $rules);

                if (!$slugControl) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('slug', 'Bu alan benzersiz olmalı.');
                    });
                }

                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }

               $content = Content::find($request->content_id);

               $contentVariable = Content::find($request->content_id)->variableLang($request->lang);
               $contentVariable->title = $request->title;
                    $contentVariable->slug = str_slug($request->slug, '-'); //SLUGGG
                    
                    if ($content->type == 'text') {
                        $contentVariable->short_content = $request->short_content;
                        $contentVariable->content = $request->content;
                    }

                    if ($content->type == 'rssfeed') {
                        $_content = array();
                        $_content['content_url'] = $request->content_url;
                        $_content['content_rowcount'] = $request->content_rowcount;
                        $contentVariable->content = json_encode($_content);
                    }

                    if ($content->type == 'form') {
                        $contentVariable->content = $request->formdata;
                    }

                    if ($content->type == 'seperator') {
                        $contentVariable->short_content = $request->short_content;
                    }

                    if ($content->type == 'link') {
                        $_content = array();
                        $_content['content_url'] = $request->content_url;
                        $_content['content_target'] = ($request->content_target == 'external') ? 'external' : 'internal' ;
                        $contentVariable->content = json_encode($_content);
                    }

                    if ($content->type == 'photo') {}


                        if ($content->type == 'photogallery') {
                            $contentVariable->short_content = $request->short_content;
                        }

                        if ($content->type == 'code') {
                            $contentVariable->short_content = $request->short_content;
                            $contentVariable->content = $request->code;
                        }


                        if ($content->type == 'group') {
                            $contentVariable->short_content = $request->short_content;
                        }

                        $contentVariable->save();
                        $text = 'Başarıyla Düzenlendi...';
                    }

                    if ($request->menu_id == 0) {
                        return redirect('form/list/')->with('message', array('text' => $text, 'status' => 'success'));
                    }else{
                        return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
                    }

                }else if($request->crud == 'delete'){

                //$content = Content::find($request->content_id);
                //dd($request->input());
                    $purl = "";

                    TopHasSub::where('top_menu_id', $request->menu_id)->where('sub_content_id', $request->content_id)->delete();

                    if(TopHasSub::where('sub_content_id', $request->content_id)->count() == 0){
                        $content = Content::find($request->content_id);
                        $content->deleted = 'yes';
                        $content->status = 'passive';
                        $content->save();
                        $purl = "/subcontent/".$content->top_content;
                    }

                    $text = 'Başarıyla Silindi...';

                return redirect('menu/content/'.$request->menu_id/*.$purl*/)->with('message', array('text' => $text, 'status' => 'success'));
            }
        }

        public function content_addexist($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($cid)){
                $content = null;
            }else{
                $content = Content::find($cid);
            }

            $menus = Menu::whereNotIn('type', ['mixed', 'menuitem', 'link', 'content'])->where('deleted', 'no')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            $contents = Content::where('deleted', 'no')->where('status', 'active')->orderBy('type', 'desc')->orderBy('id', 'asc')->get();//order patlamsı
            $form = Content::where('type', 'form')->where('deleted', 'no')->where('status', 'active')->orderBy('id', 'asc')->get();
            
            return view('menu.contentaddexist', array('menu' => $menu, 'menus' => $menus, 'content' => $content, 'contents' => $contents, 'form' => $form, 'langs' => $langs));
        }

        public function content_save_exist(Request $request)
        {
            //dd($request->input());
            $text = "";
            if($request->crud == 'addexist'){

                if($request->existcontent != 'null'){

                    if (!is_null($request->menu_id) && !is_null($request->content_id)) {

                        $top_has_sub = new TopHasSub();
                        $top_has_sub->top_menu_id = $request->menu_id;
                        $top_has_sub->top_content_id = $request->content_id;
                        $top_has_sub->sub_content_id = $request->existcontent;
                        $top_has_sub->order = 1;

                        $_props = array();
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                        $top_has_sub->props = json_encode($_props);

                        $top_has_sub->save();

                        

                    } elseif (!is_null($request->menu_id) && is_null($request->content_id)) {

                        $top_has_sub = new TopHasSub();
                        $top_has_sub->top_menu_id = $request->menu_id;
                        $top_has_sub->sub_content_id = $request->existcontent;
                        $top_has_sub->order = 1;

                        $_props = array();
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                        $top_has_sub->props = json_encode($_props);

                        $top_has_sub->save();

                    } elseif (is_null($request->menu_id) && !is_null($request->content_id)) {

                        dd("null : !null");

                    } elseif (is_null($request->menu_id) && is_null($request->content_id)) {

                        dd("null : null");

                    }

                }elseif ($request->existmenu != 'null'){

                    if (!is_null($request->menu_id) && !is_null($request->content_id)) {

                        $top_has_sub = new TopHasSub();
                        $top_has_sub->top_menu_id = $request->menu_id;
                        $top_has_sub->top_content_id = $request->content_id;
                        $top_has_sub->sub_menu_id = $request->existmenu;
                        $top_has_sub->order = 1;

                        /*

                        $_props = array();
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                        $top_has_sub->props = json_encode($_props);

                        */

                        $top_has_sub->save();

                    } elseif (!is_null($request->menu_id) && is_null($request->content_id)) {

                        $top_has_sub = new TopHasSub();
                        $top_has_sub->top_menu_id = $request->menu_id;
                        $top_has_sub->sub_menu_id = $request->existmenu;
                        $top_has_sub->order = 1;

                        if(true){
                            $_props = array();
                            $_props['props_sum_type'] = '';
                            $_props['props_sum_count'] = '';
                            $_props['props_sum_colvalue'] = 'col-lg-12';//default sütün genişliği atandı
                            $top_has_sub->props = json_encode($_props);
                        }

                        $top_has_sub->save();

                    } elseif (is_null($request->menu_id) && !is_null($request->content_id)) {

                        dd("null : !null");

                    } elseif (is_null($request->menu_id) && is_null($request->content_id)) {

                        dd("null : null");

                    }

                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }

        }
        /** CONTENT **/

        /** FORM LIST-DETAIL **/
        public function form_list()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('form.list', array('langs' => $langs));
        }

        public function form_detail($formid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $form = Content::where('id', $formid)->where('type', 'form')->where('deleted', 'no')->first();
            $formDist = FormData::select('source_id', 'source_type')->where('form_id', $form->id)->distinct()->get();

            if (!is_null($form)) {
                return view('form.detail', array('form' => $form, 'formDist' => $formDist, 'langs' => $langs));
            }
            
        }

        public function form_change_visible($formid, $dataid)
        {

            $formdata = FormData::where('id', $dataid)->orderBy('id', 'desc')->first();

            if($formdata->visible == 'yes'){
                $formdata->visible = 'no';
            }elseif($formdata->visible == 'no'){
                $formdata->visible = 'yes';
            }

            $formdata->save();

            $text = 'İşlem Başarıyla Gerçekleştirildi...';
            return redirect('form/detail/'.$formid)->with('message', array('text' => $text, 'status' => 'success'));

        }
        /** FORM LIST-DETAIL **/

        /** ADD-ON **/
        public function addons($id, $cid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }
            //$photogalleryitem = ContentPhotoGalleryVariable::find($fid);
            return view('menu.addons', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
        }

        public function addon_add_edit($id, $cid, $aid = null, $lang = null)
        {

            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $menu = Menu::find($id);
            $content = Content::find($cid);

            if(is_null($aid)){
                $addon = new Content();
            }else{
                $addon = Content::find($aid);
                if(!is_null($lang) && is_null($addon->variableLang($lang))){
                    $addonVariable = new ContentVariable();
                    $addonVariable->content_id = $addon->id;
                    $addonVariable->lang_code = $lang;
                    $addonVariable->title = '';
                    $addonVariable->save();
                }
            }
            
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_delete($id, $cid, $aid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            $addon = Content::find($aid);
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'activeLang' => $activeLang, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_save(Request $request)
        {
            //dd($request->input());
            if($request->crud == 'add'){
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
               $addon = new Content();
               $addon->top_content = $request->content_id;
               if ($request->type != 'null') {
                $addon->type = $request->type.'';
            }
            $addon->order = $request->order;
            $addon->status = ($request->status == 'active') ? 'active' : 'passive';
            $addon->save();

            foreach ($languages as $language) {
                $addonVariable = new ContentVariable();
                $addonVariable->content_id = $addon->id;
                $addonVariable->lang_code = $language->code;
                $addonVariable->title = $request->title;
                $addonVariable->save();
            }

            $text = 'Başarıyla Eklendi...';
            return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));

        }elseif($request->crud == 'edit'){
            if (is_null($request->lang)) {
                $rules = array(
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

               $addon = Content::find($request->addon_id);
               $addon->order = $request->order;
               $addon->status = ($request->status == 'active') ? 'active' : 'passive';
               $addon->save();

               if ($addon->type == 'photogallery') {
                foreach ($languages as $language) {
                    $addonVariable = Content::find($request->addon_id)->variableLang($language->code);
                    $addonVariable->props = $request->props;
                    $addonVariable->save();
                }
            }

            $tagItems = ContentHasTag::where('content_id', $request->addon_id)->delete();
            if (!is_null($request->tag)) {
                foreach ($request->tag as $tt) {
                    $tagItem = new ContentHasTag();
                    $tagItem->content_id = $request->addon_id;
                    $tagItem->tag_id = $tt;
                    $tagItem->save();
                }
            }

            $categoryItems = ContentHasCategory::where('content_id', $request->addon_id)->delete();
            if (!is_null($request->category)) {
                foreach ($request->category as $cc) {
                    $categoryItem = new ContentHasCategory();
                    $categoryItem->content_id = $request->addon_id;
                    $categoryItem->category_id = $cc;
                    $categoryItem->save();
                }
            }

            $text = 'Başarıyla Düzenlendi...';
        }else{
            $rules = array(
                'title' => 'required|max:255'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
           }
           $addon = Content::find($request->addon_id);
           $addonVariable = Content::find($request->addon_id)->variableLang($request->lang);
           $addonVariable->title = $request->title;
           if ($addon->type == 'photogallery') {
            $addonVariable->props = $request->props;
        }
        if ($addon->type == 'text') {
        }else{
            $addonVariable->row = 'normal';
            $addonVariable->height = null;
        }
        $addonVariable->save();
        $text = 'Başarıyla Düzenlendi...';
    }
    return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));

}elseif($request->crud == 'delete'){
                //dd($request->input());

                //MenuHasContent::where('menu_id', $request->menu_id)->where('content_id', $request->addon_id)->delete();
    if(MenuHasContent::where('content_id', $request->addon_id)->count() == 0){
        $addon = Content::find($request->addon_id);
        $addon->deleted = 'yes';
        $addon->status = 'passive';
        $addon->save();
        $text = 'Başarıyla Silindi...';
    }else{
        $addon = Content::find($request->addon_id);
        $addon->top_content = null;
        $addon->save();
        $text = 'Başarıyla Silindi...';
    }
    return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
}
return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
}
/** ADD-ON **/

/** TAG **/
public function tag()
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    return view('menu.tag', array('langs' => $langs));
}

public function tag_add_edit($tid = null, $lang = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    if(is_null($tid)){
        $tag = new Tag();
    }else{
        $tag = Tag::find($tid);
    }
    return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
}

public function tag_delete($tid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $tag = Tag::find($tid);
    return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
}

public function tag_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'add') {
        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
       $tag = new Tag();
       $tag->order = $request->order;
       $tag->status = ($request->status == 'active') ? 'active' : 'passive';
       $tag->save();
       foreach ($languages as $language) {
        $tagVariable = new TagVariable();
        $tagVariable->tag_id = $tag->id;
        $tagVariable->lang_code = $language->code;
        $tagVariable->title = $request->title;
        $tagVariable->save();
    }
    $text = 'Başarıyla Eklendi...';
}else if($request->crud == 'edit'){
    if (is_null($request->lang)) {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $tag = Tag::find($request->tag_id);
       $tag->order = $request->order;
       $tag->status = ($request->status == 'active') ? 'active' : 'passive';
       $tag->save();
   }else{
                    //dd($request->input());
    $rules = array(
        'title' => 'required'
    );
    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
       return \Redirect::back()->withErrors($validator)->withInput();
   }
   $tagVariable = Tag::find($request->tag_id)->variableLang($request->lang);
   if (!isset($tagVariable)) {
    $tagVariable = new TagVariable();
    $tagVariable->tag_id = $request->tag_id;
    $tagVariable->lang_code = $request->lang;
}
$tagVariable->title = $request->title;
$tagVariable->save();
}
$text = 'Başarıyla Düzenlendi...';
}else if($request->crud == 'delete'){
    $tag = Tag::find($request->tag_id);
    $tag->deleted = 'yes';
    $tag->status = 'passive';
    $tag->save();
    $text = 'Başarıyla Silindi...';
}
return redirect('tag')->with('message', array('text' => $text, 'status' => 'success'));
}
/** TAG **/

/** CATEGORY **/
public function category()
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    return view('menu.category', array('langs' => $langs));
}

public function category_add_edit($cid = null, $lang = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    if(is_null($cid)){
        $category = new Category();
    }else{
        $category = Category::find($cid);
    }
    return view('menu.categorycrud', array('category' => $category, 'langs' => $langs));
}

public function category_delete($cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $category = Category::find($cid);
    return view('menu.categorycrud', array('category' => $category, 'langs' => $langs));
}

public function category_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'add') {
        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
       $category = new Category();
       $category->order = $request->order;
       $category->status = ($request->status == 'active') ? 'active' : 'passive';
       $category->save();
       foreach ($languages as $language) {
        $categoryVariable = new CategoryVariable();
        $categoryVariable->category_id = $category->id;
        $categoryVariable->lang_code = $language->code;
        $categoryVariable->title = $request->title;
        $categoryVariable->save();
    }
    $text = 'Başarıyla Eklendi...';
}else if($request->crud == 'edit'){
    if (is_null($request->lang)) {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $category = Category::find($request->category_id);
       $category->order = $request->order;
       $category->status = ($request->status == 'active') ? 'active' : 'passive';
       $category->save();
   }else{
                    //dd($request->input());
    $rules = array(
        'title' => 'required'
    );
    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
       return \Redirect::back()->withErrors($validator)->withInput();
   }
   $categoryVariable = Category::find($request->category_id)->variableLang($request->lang);
   if (!isset($categoryVariable)) {
    $categoryVariable = new CategoryVariable();
    $categoryVariable->category_id = $request->category_id;
    $categoryVariable->lang_code = $request->lang;
}
$categoryVariable->title = $request->title;
$categoryVariable->save();
}
$text = 'Başarıyla Düzenlendi...';
}else if($request->crud == 'delete'){
    $category = Category::find($request->category_id);
    $category->deleted = 'yes';
    $category->status = 'passive';
    $category->save();
    $text = 'Başarıyla Silindi...';
}
return redirect('category')->with('message', array('text' => $text, 'status' => 'success'));
}
/** CATEGORY **/

/** PHOTO GALLERY **/
public function photogalleryitem($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $content = new Content();
    }else{
        $content = Content::find($cid);
    }
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function photogalleryitem_delete($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    $content = Content::find($cid);
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function photogalleryitem_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'photogallery_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'photogallery_edit'){

        $photogalleryitem = ContentPhotoGalleryVariable::find($request->photo_gallery_id);
        $photogalleryitem->name = $request->name;
        $photogalleryitem->description = $request->description;
        $photogalleryitem->order = $request->order;
        $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $photogalleryitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'photogallery_delete'){
    $photogalleryitem = ContentPhotoGalleryVariable::find($request->photo_gallery_id);
    $photogalleryitem->deleted = 'yes';
    $photogalleryitem->status = 'passive';
    $photogalleryitem->save();
    $text = 'Başarıyla Silindi...';
}
return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->content_id.'/'.$request->lang)->with('message', array('text' => $text, 'status' => 'success'));
}
/** PHOTO GALLERY **/

/** SLIDE GALLERY **/
public function slideitem($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $content = new Content();
    }else{
        $content = Content::find($cid);
    }
    $slideitem = ContentSlideVariable::find($fid);
    return view('menu.slideitem', array('menu' => $menu, 'content' => $content, 'slideitem' => $slideitem, 'activeLang' => $activeLang));
}

public function slideitem_delete($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    $content = Content::find($cid);
    $slideitem = ContentSlideVariable::find($fid);
    return view('menu.slideitem', array('menu' => $menu, 'content' => $content, 'slideitem' => $slideitem, 'activeLang' => $activeLang));
}

public function slideitem_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'slide_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'slide_edit'){

        $slideitem = ContentSlideVariable::find($request->slide_id);
        $slideitem->title = $request->title;
        $slideitem->description = $request->description;

        $slideitem->button_text = $request->button_text;
        $slideitem->button_url = $request->button_url;
        $slideitem->theme = $request->theme;
        $slideitem->align = $request->align;

        $slideitem->order = $request->order;
        $slideitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $slideitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'slide_delete'){
    $slideitem = ContentSlideVariable::find($request->slide_id);
    $slideitem->deleted = 'yes';
    $slideitem->status = 'passive';
    $slideitem->save();
    $text = 'Başarıyla Silindi...';
}

return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->content_id.'/'.$request->lang)->with('message', array('text' => $text, 'status' => 'success'));
}
/** SLIDE GALLERY **/

/** ADDON PHOTO GALLERY **/
public function addon_photogalleryitem($id, $cid, $aid, $fid)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $content = new Content();
    }else{
        $content = Content::find($cid);
    }
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function addon_photogalleryitem_delete($id, $cid, $aid, $fid)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    $content = Content::find($cid);
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function addon_photogalleryitem_save(Request $request)
{
    $text = "";
    if ($request->crud == 'photogallery_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'photogallery_edit'){

        $photogalleryitem = ContentPhotoGalleryVariable::find($request->fid);
        $photogalleryitem->name = $request->name;
        $photogalleryitem->description = $request->description;
        $photogalleryitem->order = $request->order;
        $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $photogalleryitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'photogallery_delete'){
    $photogalleryitem = ContentPhotoGalleryVariable::find($request->fid);
    $photogalleryitem->deleted = 'yes';
    $photogalleryitem->status = 'passive';
    $photogalleryitem->save();
    $text = 'Başarıyla Silindi...';
    return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
}
return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
}
/** ADDON PHOTO GALLERY **/

/** SLIDER **/
public function stslider($id = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $topmenus = Menu::where('deleted', 'no')->get();
    $menu = Menu::find($id);
    return view('menu.stslider', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
}

public function stslider_add_edit($id, $sid = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $slide = new Slider();
    }else{
        $slide = Slider::find($sid);
    }
    return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
}

public function stslider_delete($id, $sid = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $slide = new Slider();
    }else{
        $slide = Slider::find($sid);
    }
    return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
}

public function stslider_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if($request->crud == 'add'){

        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }

       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

       $slide = new Slider();
       $slide->menu_id = $request->menu_id;
       $slide->theme = $request->theme;
       $slide->align = $request->align;
       $slide->order = $request->order;
       $slide->status = ($request->status == 'active') ? 'active' : 'passive';
       $slide->save();

       foreach ($languages as $language) {
        $slideVariable = new SliderVariable();
        $slideVariable->slider_id = $slide->id;
        $slideVariable->lang_code = $language->code;
        $slideVariable->title = $request->title;
        $slideVariable->description = $request->description;
        $slideVariable->button_text = $request->button_text;
        $slideVariable->button_url = $request->button_url;
        $slideVariable->save();
    }

    $text = 'Başarıyla Eklendi...';
    return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}else if($request->crud == 'edit'){

    if (is_null($request->lang)) {

        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }

       $slide = Slider::where('id', $request->slider_id)->first();
       $slide->theme = $request->theme;
       $slide->align = $request->align;
       $slide->order = $request->order;
       $slide->status = ($request->status == 'active') ? 'active' : 'passive';
       $slide->save();

   }else{

    $rules = array(
        'title' => 'required|max:255'
    );
    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
       return \Redirect::back()->withErrors($validator)->withInput();
   }

   $slide = Slider::where('id', $request->slider_id)->first();
   $slideVariable = $slide->variableLang($request->lang);
   $slideVariable->title = $request->title;
   $slideVariable->description = $request->description;
   $slideVariable->button_text = $request->button_text;
   $slideVariable->button_url = $request->button_url;
   $slideVariable->save();

}
return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}else if($request->crud == 'delete'){

    $slide = Slider::find($request->slider_id);
    $slide->deleted = 'yes';
    $slide->status = 'passive';
    $slide->save();
    $text = 'Başarıyla Silindi...';
    return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}
}
/** SLIDER **/

/** MAP MARKER **/
public function mapmarker($id, $cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);

    if(is_null($id)){
        $content = new Content();
    }else{
        $content = Content::find($cid);
    }

    return view('menu.mapmarker', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
}

public function mapmarker_add($id, $cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $content = Content::find($cid);
    $staff = new Staff();

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_edit($id, $cid, $sid, $lang = null)
{
            //dd($id.' - '.$cid);
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    $content = Content::find($cid);
    $staff = Staff::find($sid);

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_delete($id, $cid, $sid, $lang = null)
{
            //dd($id.' - '.$cid);
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    $content = Content::find($cid);
    $staff = Staff::find($sid);

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if($request->crud == 'add'){

        $rules = array(
            'name' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

       $menu = Menu::find($request->menu_id);
       $content = Content::find($request->content_id);

       $staff = new Staff();
       $staff->content_id = $request->content_id;

       if ($request->district == 'null') { 
        $staff->district_id = null; 
    }else{
        $staff->district_id = $request->district; 
    }

    if ($request->city == 'null') { 
        $staff->city_id = null; 
    }else{
        $staff->city_id = $request->city; 
    }

    if ($request->county == 'null') { 
        $staff->county_id = null; 
    }else{
        $staff->county_id = $request->county; 
    }

    $staff->order = $request->order;
    $staff->status = ($request->status == 'active') ? 'active' : 'passive';

    $staff->save();

    foreach ($languages as $language) {

        $staffVariable = new StaffVariable();
        $staffVariable->staff_id = $staff->id;
        $staffVariable->lang_code = $language->code;
        $staffVariable->name = $request->name;
                    $staffVariable->slug = str_slug($request->name, '-');  //SLUGGG
                    $staffVariable->title = $request->ptitle;
                    $staffVariable->address = $request->address;
                    $staffVariable->email = $request->email;
                    $staffVariable->phone = $request->phone;
                    $staffVariable->gsm = $request->gsm;

                    $staffVariable->photo_url = '';
                    $staffVariable->save();
                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id.'/mapmarker/'.$request->content_id)->with('message', array('text' => $text, 'status' => 'success')); 

            }else if($request->crud == 'edit'){

                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }

                   $menu = Menu::find($request->menu_id);
                   $content = Content::find($request->content_id);
                   $staff = Staff::find($request->staff_id);

                   $staff->order = $request->order; 
                   $staff->status = ($request->status == 'active') ? 'active' : 'passive';
                   $staff->save();

                   $text = 'Başarıyla Düzenlendi...';
               }else{
                    //dump($request->input());
                $rules = array(
                    'name' => 'required|max:255'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }

               $menu = Menu::find($request->menu_id);
               $content = Content::find($request->content_id);
               $staff = Staff::find($request->staff_id);

               if ($request->district == 'null') { 
                $staff->district_id = null; 
            }else{
                $staff->district_id = $request->district; 
            }

            if ($request->city == 'null') { 
                $staff->city_id = null; 
            }else{
                $staff->city_id = $request->city; 
            }

            if ($request->county == 'null') { 
                $staff->county_id = null; 
            }else{
                $staff->county_id = $request->county; 
            }
            $staff->save();

            $staffVariable = $staff->variableLang($request->lang);
            $staffVariable->name = $request->name;
                    $staffVariable->slug = str_slug($request->name, '-');  //SLUGGG
                    $staffVariable->title = $request->ptitle;
                    $staffVariable->address = $request->address;
                    $staffVariable->email = $request->email;
                    $staffVariable->phone = $request->phone;
                    $staffVariable->gsm = $request->gsm;
                    $staffVariable->save();

                    $text = 'Başarıyla Düzenlendi...';
                }

                return redirect('menu/content/'.$request->menu_id.'/mapmarker/'.$request->content_id)->with('message', array('text' => $text, 'status' => 'success')); 
                
            }else if($request->crud == 'delete'){

                $menu = Menu::find($request->menu_id);
                $content = Content::find($request->content_id);
                $staff = Staff::find($request->staff_id);

                $staff->deleted = 'yes';
                $staff->status = 'passive';
                $staff->save();

                $text = 'Başarıyla Silindi...';
                return redirect('menu/content/'.$request->menu_id.'/mapmarker/'.$request->content_id)->with('message', array('text' => $text, 'status' => 'success')); 
            }
        }
        /** MAP MARKER **/

        public function stimage($id)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $topmenus = Menu::where('deleted', 'no')->get();
            $menu = Menu::find($id);
            return view('menu.stimage', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
        }

    }
