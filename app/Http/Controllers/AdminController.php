<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use App\Language;
use App\Menu;
use App\Content;
use App\ContentVariable;
use App\ContentSlideVariable;
use App\ContentPhotoGalleryVariable;
use App\Tag;
use App\Category;
use App\Slider;
use App\Calendar;
use App\FormData;
use App\TopHasSub;

use App\District;
use App\DistrictVariable;
use App\Staff;
use App\StaffVariable;

use App\Country;
use App\CountryVariable;
use App\City;
use App\CityVariable;
use App\County;
use App\CountyVariable;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    private $photos_path;

    public function __construct()
    {
        $this->middleware('auth');
        $this->photos_path = public_path(config('webshell.upload.path'));
    }

    public function index()
    {
        /*
        $menu = Menu::where('deleted', 'no')->where('id', 1)->first();
        $content = $menu->content()->where('deleted', 'no');
        $content = $content->orderBy('id', 'desc')->get();
        dd($content);
        */
        return view('dashboard');
    }

    public function getMenuAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'status', 'name', 'slug', 'top_id', 'type',  'actions');
        
        $menus = Menu::where('deleted', 'no');
        $menus = $menus->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $menus->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $params['slidertype'] = $menus[$i]->slidertype;
            $records["data"][] = array(
                $menus[$i]->id,
                $menus[$i]->order,
                $menus[$i]->status,
                $menus[$i]->variable->name,
                $menus[$i]->variable->slug,
                is_null($menus[$i]->top_id) ? '' : $menus[$i]->topMenu->variable->name,
                $menus[$i]->type,
                $params,
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getContentAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        $order_by = array('id', 'order', 'title', 'type', 'status', 'actions');

        $menu = Menu::where('deleted', 'no')->where('id', $_REQUEST['menuId'])->first();
        
        if (empty($_REQUEST['contentId'])) {
            //$contents = $menu->menuHasContent()->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
            $contents = $menu->topHasSub()->whereNull('top_content_id')->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        }else{
            $contents = $menu->topHasSub()->where('top_content_id', $_REQUEST['contentId'])->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
            //$contents = Content::where('deleted', 'no')->where('top_content', $_REQUEST['contentId']);
            //$contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        }

        //dd($contents);

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            if(!is_null($contents[$i]->sub_content_id)){
                $cont = $contents[$i]->subContent;
                $title = $cont->variable->title;
                $ttype = 'content';
            }else if(!is_null($contents[$i]->sub_menu_id)){
                $cont = $contents[$i]->subMenu;
                $title = $cont->variable->name;
                $ttype = 'menu';
            }
            $type = $cont->type.'##'.$ttype;

            $records["data"][] = array(
                $cont->id,
                $contents[$i]->order,
                $title,//.' - '.$_REQUEST['contentId'],
                $cont->variable->slug,
                $type,
                $cont->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getTagAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'status', 'actions');

        $tags = Tag::where('deleted', 'no');
        $tags = $tags->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();    
        
        $iTotalRecords = $tags->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] = array(
                $tags[$i]->id,
                $tags[$i]->order,
                $tags[$i]->variable->title,
                $tags[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getCategoryAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'status', 'actions');

        $categories = Category::where('deleted', 'no');
        $categories = $categories->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $categories->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] = array(
                $categories[$i]->id,
                $categories[$i]->order,
                $categories[$i]->variable->title,
                $categories[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getContentAddonsAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'type',  'status', 'actions');

        $contents = Content::where('deleted', 'no')->where('top_content', $_REQUEST['contentId']);
        $contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $contents[$i]->id,
                $contents[$i]->order,
                $contents[$i]->variable->title,
                $contents[$i]->type,
                $contents[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getSliderAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id', 'order', 'title', 'status', 'actions');
        
        $sliders = Slider::where('deleted', 'no')->where('menu_id', $_REQUEST['menuId']);
        $sliders = $sliders->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $sliders->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $sliders[$i]->id,
                $sliders[$i]->order,
                $sliders[$i]->variable->title,
                $sliders[$i]->status,
                ''
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPhotoGalleryAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'type', 'status', 'actions');

        $contents = Content::where('deleted', 'no')->where('type', 'photogallery');
        $contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        
        for ($i = $iDisplayStart; $i < $end; $i++) {

            $menuRoad = '';
            $menuID = null;
            
            $menuitem = $contents[$i]->menu()->first();
            //dump($contents[$i]);
            
            if(!is_null($menuitem)){

                $menuRoad = $menuitem->variableLang($activeLang->code)->name;
                $menuID = $menuitem->id;
                
                /*
                    while ($menuitem->top_id != null) {
                        $menuitem = $menuitem->topMenu;
                        $menuRoad = $menuitem->variableLang($activeLang->code)->name . ' > ' . $menuRoad;
                    }
                */

                }

                $records["data"][] = array(
                    $contents[$i]->id,
                    $contents[$i]->order,
                    $contents[$i]->variable->title,
                    $menuRoad,
                    $contents[$i]->status,
                    $menuID
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

        public function getCalendarAjax()
        {
            foreach ($_REQUEST['columns'] as $column) {
                $arrayName[] = $column['search']['value'];
            }

            $order_by = array('id', 'order', 'status', 'title', 'start', 'end', 'capacity', 'price', 'actions');

            $calendar = Calendar::where('deleted', 'no');
            $calendar = $calendar->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

            $iTotalRecords = $calendar->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            for ($i = $iDisplayStart; $i < $end; $i++) {

                $endDatee = '';
                if (!empty($calendar[$i]->end)) {
                    $endDatee = Carbon::parse($calendar[$i]->end)->format('d/m/Y H:i');
                }

                $capp = '';
                if($calendar[$i]->capacity > 0){
                    $capp = $calendar[$i]->capacity;
                }elseif($calendar[$i]->capacity == 0){
                    $capp = 'Sınırsız';
                }elseif($calendar[$i]->capacity == -1){
                    $capp = 'Kapalı Grup';
                }

                $records["data"][] = array(
                    $calendar[$i]->id,
                    $calendar[$i]->order,
                    $calendar[$i]->status,
                    $calendar[$i]->variable->title,
                    Carbon::parse($calendar[$i]->start)->format('d/m/Y H:i'),
                    $endDatee,
                    $capp,
                    ($calendar[$i]->price == 0) ? 'Ücretsiz' : $calendar[$i]->price,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

        public function getFormAjax()
        {
            foreach ($_REQUEST['columns'] as $column) {
                $arrayName[] = $column['search']['value'];
            }

            $order_by = array('id', 'order', 'status', 'title', 'actions');

            $form = Content::where('type', 'form')->where('deleted', 'no');
            $form = $form->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

            $iTotalRecords = $form->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            for ($i = $iDisplayStart; $i < $end; $i++) {

            //$uniqForms = FormData::select('source_id', 'source_type', 'form_id')->distinct()->get();
                $menuIDsTemp = array();
            //dd($form[$i]->topContentThs);
                foreach ($form[$i]->topContentThs as $ths) {
                //dd($ths->topMenu);
                    $menuIDsTemp[] = $ths->topMenu->id;
                }
                $menuIDs = array_unique($menuIDsTemp);

                $calendarIDs = array();
                foreach (Calendar::where('form_id', $form[$i]->id)->distinct()->get() as $calendar) {
                    $calendarIDs[] = $calendar->id;
                }

                $records["data"][] = array(
                    $form[$i]->id,
                    $form[$i]->order,
                    $form[$i]->status,
                $form[$i]->variable->title, //.' - '.json_encode($menuIDs).' - '.json_encode($calendarIDs),
            );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

        public function getFormDataAjax()
        {

            foreach ($_REQUEST['columns'] as $column) {
                $arrayName[] = $column['search']['value'];
            }

            $form = Content::where('id', $_REQUEST['formId'])->where('type', 'form')->where('deleted', 'no')->first();
            $formvariable = $form->variable()->first();
            $formvariableArr = json_decode($formvariable->content);

            $order_by = array('id');

            foreach($formvariableArr as $field){
                if(isset($field->name)){
                    $order_by[] = $field->name;
                }
            }

            $order_by[] = 'actions';

            if(empty($_REQUEST['search']['value'])){
                $formdata = FormData::where('form_id', $_REQUEST['formId'])->orderBy('id', 'desc')->get();
            }elseif($_REQUEST['search']['value'] == 'null'){
                $formdata = FormData::where('form_id', $_REQUEST['formId'])->orderBy('id', 'desc')->get();
            }else{
                $arr = explode(':',$_REQUEST['search']['value']);
                $formdata = FormData::where('form_id', $_REQUEST['formId'])->where('source_type', $arr[0])->where('source_id', $arr[1])->orderBy('id', 'desc')->get();
            }
        //$formdata = $form->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

            $iTotalRecords = $formdata->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            for ($i = $iDisplayStart; $i < $end; $i++) {

                $records["data"][] = array($formdata[$i]->id);
                $formDataArr = json_decode($formdata[$i]->data);

                foreach($formDataArr as $fieldname => $value){
                    if(starts_with($fieldname, 'date')){
                        $records["data"][count($records["data"])-1][] = Carbon::parse($value)->format('d.m.Y');
                    }else{
                        $records["data"][count($records["data"])-1][] = $value;
                    }
                }

                $sourceData = '';

                if($formdata[$i]->source_type == 'calendar'){
                    $sourceData = 'Etkinlik - '.Calendar::find($formdata[$i]->source_id)->variable->title;
                }else if($formdata[$i]->source_type == 'menu'){
                    $source = Menu::find($formdata[$i]->source_id);
                    if($formdata[$i]->lang_code=="tr"){
                        $sourceData ='<span class="m-badge m-badge--danger m-badge--wide">'.$formdata[$i]->lang_code.'</span> Menü - '.$source->variable->name;
                    }
                    else if($formdata[$i]->lang_code=="en"){
                        $sourceData ='<span class="m-badge m-badge--primary m-badge--wide">'.$formdata[$i]->lang_code.'</span> Menü - '.$source->variable->name;
                    }
                    $ths = TopHasSub::where('top_menu_id', $source->id)->where('sub_content_id', $form->id)->first();
                }else if($formdata[$i]->source_type == 'content'){
                    $source = Content::find($formdata[$i]->source_id);
                    $sourceData = 'İçerik - '.$source->variable->title;
                    $ths = TopHasSub::where('top_content_id', $source->id)->where('sub_content_id', $form->id)->first();
                }

            //dd(json_decode($ths->props));

                if(json_decode($ths->props)->props_comment_status == 'active'){
                    $form_visible = $formdata[$i]->visible;
                }else{
                    $form_visible = 'passive';
                }


                $records["data"][count($records["data"])-1][] = $sourceData;
                $records["data"][count($records["data"])-1][] = $form_visible;
                $records["data"][count($records["data"])-1][] = '';

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }

        public function uploadFile(Request $request)
        {
        //dd($request->input());
            $photos = $request->file('file');
            if (!is_array($photos)) {
                $photos = [$photos];
            }

            $uploadConfig = config('webshell.upload.imageStandart');

            if(!File::exists($this->photos_path)) { File::makeDirectory($this->photos_path); }

            foreach ($uploadConfig as $key => $value) {
                if(!File::exists($this->photos_path.'/'.$key)) { File::makeDirectory($this->photos_path.'/'.$key); }
            }

            for ($i = 0; $i < count($photos); $i++) {

                $photo = $photos[$i];
                $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->extension();

                foreach ($uploadConfig as $key => $conf) {

                    $imageObj = Image::make($photo);

                    if ($conf['widen']) {
                        $imageObj->widen($conf['width'], function ($constraint) {
                            $constraint->upsize();
                        });
                    }

                    $ext=$photo->getClientOriginalExtension();
                    if($ext=="png"){
                        $imageObj->save($this->photos_path . '/' . $key . '/' . $fileName,93);

                    }
                    else{
                        $imageObj->save($this->photos_path . '/' . $key . '/' . $fileName, 93,'jpg');

                    }

                /*
                    if ($key == 'thumbnail169'){
                        Image::make($photo)
                        ->widen($value, function ($constraint) {
                            $constraint->upsize();
                        })
                        ->crop($value, ($value/16*9))
                        ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                    }elseif ($key == 'thumbnail2x' || $key == 'thumbnail3x' || $key == 'thumbnail4x') {
                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();

                        if($imgW >= $imgH){
                            $imgW = $imgH;
                        }else{
                            $imgH = $imgW;
                        }

                        Image::make($photo)
                        ->crop($imgW, $imgH)
                        ->widen($value, function ($constraint) {
                            $constraint->upsize();
                        })
                        ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                    }elseif($key == 'thumbnail32'){

                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();
                        $imgOran = $imgW/$imgH;
                        if ($imgOran < (600/400)) {
                            Image::make($photo)
                            ->widen(600)
                            ->crop(600,400)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }else{
                            Image::make($photo)
                            ->heighten(400)
                            ->crop(600,400)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }

                    }elseif($key == 'slide'){
                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();
                        $imgOran = $imgW/$imgH;
                        if ($imgOran < (1200/450)) {
                            Image::make($photo)
                            ->widen(1200)
                            ->crop(1200,450)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }else{
                            Image::make($photo)
                            ->heighten(420)
                            ->crop(975,420)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }
                    }elseif($key == 'calendar'){
                        
                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();
                        $imgOran = $imgW/$imgH;
                        if ($imgOran < (731/1024)) {
                            Image::make($photo)
                            ->widen(731)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }else{
                            Image::make($photo)
                            ->heighten(1024)
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        } 
                    }elseif($key == 'stimage'){
                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();
                        $imgOran = $imgW/$imgH;
                        if ($imgOran < ($value['w']/$value['h'])) {
                            Image::make($photo)
                            ->widen($value['w'])
                            ->crop($value['w'],$value['h'])
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }else{
                            Image::make($photo)
                            ->heighten($value['h'])
                            ->crop($value['w'],$value['h'])
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }
                    }elseif($key == 'stslider'){
                        $imgW = Image::make($photo)->width();
                        $imgH = Image::make($photo)->height();
                        $imgOran = $imgW/$imgH;
                        if ($imgOran < ($value['w']/$value['h'])) {
                            Image::make($photo)
                            ->widen($value['w'])
                            ->crop($value['w'],$value['h'])
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }else{
                            Image::make($photo)
                            ->heighten($value['h'])
                            ->crop($value['w'],$value['h'])
                            ->save($this->photos_path . '/' . $key . '/' . $fileName, 93);
                        }
                    }else{
                        Image::make($photo)
                        ->widen($value, function ($constraint) {
                            $constraint->upsize();
                        })
                        ->save($this->photos_path . '/' . $key . '/' . $fileName, 93); 
                    }
                */

                }

                $photo->move($this->photos_path . '/org/' , $fileName);

                if ($request->uploadType == 'contentimage') {

                    if(isset($request->contentid)){
                        $content = Content::find($request->contentid);
                        $contentVariable = $content->variableLang($request->lang_code);
                        $contentVariable->content = $fileName;
                        $contentVariable->save();
                        echo json_encode(array('location' => $fileName, 'id' => $contentVariable->id));
                        die;
                    }

                }else if ($request->uploadType == 'galleryimage') {

                    if(isset($request->contentid)){
                        $contentPhotoGalleryVariable = new ContentPhotoGalleryVariable();
                        $contentPhotoGalleryVariable->content_id = $request->contentid;
                        $contentPhotoGalleryVariable->lang = $request->lang_code;
                        $contentPhotoGalleryVariable->url = $fileName;
                        $contentPhotoGalleryVariable->order = 1000;
                        $contentPhotoGalleryVariable->save();
                        echo json_encode(array('location' => $fileName, 'id' => $contentPhotoGalleryVariable->id));
                        die;
                    }

                }else if ($request->uploadType == 'slideimage') {

                    if(isset($request->contentid)){
                        $content = Content::find($request->contentid);

                        $contentSlide = new ContentSlideVariable();
                        $contentSlide->content_id  = $request->contentid;
                        $contentSlide->lang_code = $request->lang_code;
                        $contentSlide->title = '';
                        $contentSlide->image_url = $fileName;
                        $contentSlide->order = 1000;
                        $contentSlide->save();

                        echo json_encode(array('location' => $fileName, 'id' => $contentSlide->id));
                        die;
                    }

                }else if ($request->uploadType == 'stimage') {

                    if(isset($request->menuid)){
                        $menu = Menu::find($request->menuid);
                        $menuVariable = $menu->variableLang($request->lang_code);
                        $menuVariable->stvalue = $fileName;
                        $menuVariable->save();

                        echo json_encode(array('location' => $fileName, 'id' => $menuVariable->id));
                        die;
                    }

                }else if ($request->uploadType == 'stslider') {

                    $slide = Slider::find($request->slider_id);
                    $slideVariable = $slide->variableLang($request->lang_code);
                    $slideVariable->image_url = $fileName;
                    $slideVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $slideVariable->id));
                    die;

                }else if ($request->uploadType == 'calendarimage') {

                    if(isset($request->calendarid)){
                        $calendarVariable = Calendar::find($request->calendarid)->variableLang($request->lang_code);
                        $calendarVariable->image_name = $fileName;
                        $calendarVariable->save();
                        echo json_encode(array('location' => $fileName, 'id' => $calendarVariable->id));
                        die;
                    }

                }else if ($request->uploadType == 'contentbgimage') {
                    if(isset($request->contentid)){
                        $content = Content::find($request->contentid);
                        $contentVariable = $content->variableLang($request->lang_code);
                        $contentVariable->bgimageurl = $fileName;
                        $contentVariable->save();
                        echo json_encode(array('location' => $fileName));
                        die;
                    }
                }else if ($request->uploadType == 'editor') {
                    echo json_encode(array('location' => url('upload/xlarge/'.$fileName)));
                    die;
                }else if ($request->uploadType == 'staffimage') {

                    if(isset($request->contentid)){
                        $staff = Staff::find($request->contentid);
                        $staffVariable = $staff->variableLang($request->lang_code);
                        $staffVariable->photo_url = $fileName;
                        $staffVariable->save();
                        echo json_encode(array('location' => $fileName, 'id' => $staffVariable->id));
                        die;
                    }
                }

            }
        }

        public function cropThumbnail(Request $request)
        {
        //dd($request->input());
            if($request->input('uploadType') == 'mapturkey'){
                $uploadConfig = config('webshell.upload.imageThumbnail.thumbnailStaff');
            }else{
                $uploadConfig = config('webshell.upload.imageThumbnail.thumbnail');
            }

            if(!File::exists($this->photos_path)) { File::makeDirectory($this->photos_path); }
            if(!File::exists($this->photos_path.'/thumbnail')) { File::makeDirectory($this->photos_path.'/thumbnail'); }

            $photo = $request->file('file');

            if ($request->uploadType == 'photo') {
                $photoRow = ContentVariable::find($request->input('id'));
                $fileName = $photoRow->content;
            }else if ($request->uploadType == 'photogallery') {
                $photoRow = ContentPhotoGalleryVariable::find($request->input('id'));
                $fileName = $photoRow->url;
            }else if ($request->uploadType == 'mapturkey') {
                $photoRow = Staff::find($request->input('id'))->variableLang($request->lang_code);
                $fileName = $photoRow->photo_url;
            }

            $imageObj = Image::make($photo);

            if ($uploadConfig['widen']) {
                $imageObj->widen($uploadConfig['width'], function ($constraint) {
                    $constraint->upsize();
                });
            }

        /*
            if ($uploadConfig['crop']) {

                if($uploadConfig['height'] == 0){
                    $imageObj->crop($uploadConfig['width'], ($uploadConfig['width']/$uploadConfig['aspectRatio']));
                }else{
                    $imageObj->crop($uploadConfig['width'], $uploadConfig['height']);
                }
                
            }
        */

            $imageObj->save($this->photos_path . '/thumbnail/' . $fileName, 93);

            if ($request->uploadType == 'photo') {
                $photoRow->content = $fileName;
            }else if ($request->uploadType == 'photogallery') {
                $photoRow->url = $fileName;
            }

            $photoRow->save();
            echo json_encode(array('location' => $fileName, 'id' => $photoRow->id));
            die;

        }

        public function getMapMarkerAjax()
        {
            foreach ($_REQUEST['columns'] as $column) {
                $arrayName[] = $column['search']['value'];
            }
            $order_by = array('id', 'order', 'name', 'ptitle', 'district_id', 'city_id', 'status', 'actions');

            $menu = Menu::where('deleted', 'no')->where('id', $_REQUEST['menuId'])->first();
            $content = Content::where('deleted', 'no')->where('id', $_REQUEST['contentId'])->first();

        //DB::enableQueryLog();
            $mapmarkers = Staff::where('deleted', 'no')->where('content_id', $_REQUEST['contentId']);
            $mapmarkers = $mapmarkers->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        //dd(DB::getQueryLog());

            $iTotalRecords = $mapmarkers->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            for ($i = $iDisplayStart; $i < $end; $i++) {

                $records["data"][] = array(
                    $mapmarkers[$i]->id,
                    $mapmarkers[$i]->order,
                    $mapmarkers[$i]->variable->name,
                    config('webshell.map_ptitle.'.$mapmarkers[$i]->variable->title.'.title'),
                    (is_null($mapmarkers[$i]->district_id)) ? '' : $mapmarkers[$i]->district->variable->name,
                    ((is_null($mapmarkers[$i]->city)) ? '' : $mapmarkers[$i]->city->variable->name).((is_null($mapmarkers[$i]->county)) ? '' : ' - '.$mapmarkers[$i]->county->variable->name),
                    $mapmarkers[$i]->status
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    }
