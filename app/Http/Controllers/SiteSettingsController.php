<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;
use App\SiteSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use File;

class SiteSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$sitesettings = SiteSettings::orderBy('order', 'asc')->get();
        return view('sitesettings', array('sitesettings' => $sitesettings));
    }

    public function save(Request $request)
    {	
    	foreach ($request->input() as $key => $value) {
    		$sitesettings = SiteSettings::where('slug', $key)->first();
    		if (!is_null($sitesettings)) {
    			$sitesettings->value = $value;
    			$sitesettings->save();
    		}
    	}
    	$text = 'Başarıyla Kaydedildi...';
      return redirect('sitesettings')->with('message', array('text' => $text, 'status' => 'success'));
  }

  public function imageCleaner(Request $request){

    $filesInFolder = \File::files('upload/org');
    foreach($filesInFolder as $file) { 
        $flagdeleted=1;

        $testvalue=$file->getFilename();
        $tables = DB::select('SHOW TABLES');
        foreach($tables as $table){
            $columns = Schema::getColumnListing($table->Tables_in_denizhan_webshell);

            $matches = DB::table("$table->Tables_in_denizhan_webshell");

            foreach($columns as $column){
                $matches = $matches->orWhere($column, 'like','%'.$testvalue.'%');
            }
            $matches = $matches->count();
            if($matches!=0){
                        $flagdeleted=0;//silinmeyecek

                        break;
                    }
                    

                }

                if($flagdeleted==1){
                    File::delete('upload/large/'.$testvalue);
                    File::delete('upload/medium/'.$testvalue);
                    File::delete('upload/org/'.$testvalue);
                    File::delete('upload/small/'.$testvalue);
                    File::delete('upload/thumbnail/'.$testvalue);
                    File::delete('upload/xlarge/'.$testvalue);


                }


            }


            $text = 'Fotoğraflar Başarı ile temizlendi';
            return redirect('sitesettings')->with('message', array('text' => $text, 'status' => 'success'));
        }


        public function cacheCleaner(Request $request){

         Artisan::call('cache:clear');
         Artisan::call('view:clear');
         Artisan::call('route:clear');
         Artisan::call('optimize');

         $text = 'Cache temizlendi!';

         return redirect('sitesettings')->with('message', array('text' => $text, 'status' => 'success'));


     }

 }
