<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountyVariable extends Model
{
    protected $table = 'map_countyvariable';
}
