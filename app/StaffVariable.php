<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffVariable extends Model
{
    protected $table = 'map_staffvariable';
}
