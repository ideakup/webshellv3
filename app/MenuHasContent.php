<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuHasContent extends Model
{
    protected $table = 'menu_has_content';

    public function content()
    {
        return $this->hasOne('App\Content', 'id', 'content_id');
    }

    public function menu()
    {
        return $this->hasOne('App\Menu', 'id', 'content_id');
    }
}
