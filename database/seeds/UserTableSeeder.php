<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $dev_user = DB::table('users')->insertGetId([
            'name' => 'Web Shell',
            'email' => 'admin@webshell.com',
            'password' => bcrypt('qweqwe'),
            'status' => 'active',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_id' => $dev_user,
            'model_type' => 'App\User'
        ]);
        
    }
}
