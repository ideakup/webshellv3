<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
    	Eloquent::unguard();

    	$this->call(PermissionTableSeeder::class);
    	$this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(LanguageTableSeeder::class);
        $this->call(SiteSettingsTableSeeder::class);

        /*
        $path = 'database/seeds/webshell_default_data.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Default Data Seeded! - OK');
        */

        $path = 'database/seeds/map_country.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Country Data Seeded! - OK');

        $path = 'database/seeds/map_city.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('City Data Seeded! - OK');

        $path = 'database/seeds/map_county.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('County Data Seeded! - OK');

    }
}
