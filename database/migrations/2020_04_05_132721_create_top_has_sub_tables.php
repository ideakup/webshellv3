<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopHasSubTables extends Migration
{
    
    public function up()
    {
        Schema::create('top_has_sub', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('top_menu_id')->unsigned()->nullable();
            $table->integer('top_content_id')->unsigned()->nullable();
            $table->integer('sub_menu_id')->unsigned()->nullable();
            $table->integer('sub_content_id')->unsigned()->nullable();
            $table->text('props')->nullable();
            $table->integer('order');
            $table->timestamps();

            /*
            $table->string('top_type', 16)->default('menu')->comment('menu, content');
            $table->integer('top_id')->unsigned()->nullable();
            $table->string('sub_type', 16)->default('content')->comment('menu, content');
            $table->integer('sub_id')->unsigned()->nullable();
            */
        });

        Schema::table('top_has_sub', function (Blueprint $table) {
            $table->foreign('top_menu_id')->references('id')->on('menu');
            $table->foreign('top_content_id')->references('id')->on('content');
            $table->foreign('sub_menu_id')->references('id')->on('menu');
            $table->foreign('sub_content_id')->references('id')->on('content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('top_has_sub');
    }
}
