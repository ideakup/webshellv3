<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTables extends Migration
{
    
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned()->nullable();
            $table->string('theme', 16)->default('light')->comment('light, dark');
            $table->string('align', 16)->default('left')->comment('left, center, right');
            
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('slider', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menu');
        });

        Schema::create('slidervariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('slider_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('title', 191);
            $table->text('description')->nullable();
            $table->string('button_text', 191)->nullable();
            $table->string('button_url', 191)->nullable();
            $table->string('image_url', 191)->nullable();
            $table->timestamps();
        });

        Schema::table('slidervariable', function (Blueprint $table) {
            $table->foreign('slider_id')->references('id')->on('slider');
        });

    }

    public function down()
    {
        Schema::dropIfExists('slidervariable');
        Schema::dropIfExists('slider');
    }
}
