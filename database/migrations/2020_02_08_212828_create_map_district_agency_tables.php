<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapDistrictAgencyTables extends Migration
{
    
    public function up()
    {
        Schema::create('map_district', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::create('map_districtvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('district_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('name', 191);
            $table->string('slug', 191);
            $table->timestamps();
        });

        Schema::table('map_districtvariable', function (Blueprint $table) {
            $table->foreign('district_id')->references('id')->on('map_district');
        });


        Schema::create('map_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('county_id')->unsigned()->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('map_staff', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
            $table->foreign('district_id')->references('id')->on('map_district');
            $table->foreign('country_id')->references('id')->on('map_country');
            $table->foreign('city_id')->references('id')->on('map_city');
            $table->foreign('county_id')->references('id')->on('map_county');
        });

        Schema::create('map_staffvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('name', 191);
            $table->string('slug', 191);

            $table->mediumText('decription')->nullable();
            $table->string('title', 191);
            $table->string('photo_url', 191)->nullable();

            $table->mediumText('address')->nullable();
            $table->string('email',191)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('gsm', 20)->nullable();
            $table->timestamps();
        });

        Schema::table('map_staffvariable', function (Blueprint $table) {
            $table->foreign('staff_id')->references('id')->on('map_staff');
        });
    }

    public function down()
    {
        Schema::dropIfExists('map_staffvariable');
        Schema::dropIfExists('map_staff');
        Schema::dropIfExists('map_districtvariable');
        Schema::dropIfExists('map_district');
    }
}
