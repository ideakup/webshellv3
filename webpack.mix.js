let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.copy('resources/assets/metronic/dist/default/assets/app/media/img', 'public/img');
//mix.copy('resources/assets/metronic/dist/default/assets/demo/default/media/img', 'public/img');
//mix.copy('resources/assets/metronic/dist/default/assets/vendors/base/images/', 'public/css/images');

mix.copy('resources/assets/metronic/dist/default/assets/vendors/base/fonts', 'public/css/fonts');

mix.copy('resources/assets/project_images', 'public/images');
mix.copy('node_modules/jquery-colorbox/example1/images', 'public/css/images');
mix.copy('resources/assets/custom/css/tinymce_inline.css', 'public/css');

mix.copy('resources/assets/upload', 'public/upload');

mix.combine([
    'resources/assets/metronic/dist/default/assets/vendors/base/vendors.bundle.js',
    'resources/assets/metronic/dist/default/assets/demo/default/base/scripts.bundle.js',
    'resources/assets/metronic/dist/default/assets/vendors/custom/datatables/datatables.bundle.js',
    'resources/assets/metronic/dist/default/assets/demo/default/custom/crud/forms/widgets/bootstrap-switch.js',
    'node_modules/jquery-colorbox/jquery.colorbox.js',
    'node_modules/moment/min/moment.min.js',
    'node_modules/moment/locale/tr.js',
    'node_modules/fullcalendar/dist/fullcalendar.js',
    'node_modules/fullcalendar/dist/locale/tr.js',
    'node_modules/2mundos-fengyuanchen-cropperjs/dist/cropper.js',
    'resources/assets/custom/js/date-sorting-datatables.js',
], 'public/js/appm.js');//.version();


mix.combine([
    'node_modules/jquery-ui-sortable/jquery-ui.min.js',
    'node_modules/formBuilder/dist/form-builder.min.js',
    'node_modules/formBuilder/dist/form-render.min.js',
], 'public/js/formBuilder.js');//.version();


mix.combine([
    'resources/assets/metronic/dist/default/assets/vendors/custom/datatables/datatables.bundle.css',
    'resources/assets/metronic/dist/default/assets/vendors/base/vendors.bundle.css',
    'resources/assets/metronic/dist/default/assets/demo/default/base/style.bundle.css',
    'node_modules/jquery-colorbox/example1/colorbox.css',
    'node_modules/fullcalendar/dist/fullcalendar.css',
    'node_modules/2mundos-fengyuanchen-cropperjs/dist/cropper.css',
    'resources/assets/custom/css/custom.css',
], 'public/css/appm.css');//.version();

mix.combine([
    'resources/assets/custom/js/login.js',
], 'public/js/auth.js');//.version();

mix.combine([
    'resources/assets/custom/js/resetpassword.js',
], 'public/js/resetpass.js');//.version();

mix.combine([
    'resources/assets/custom/js/custom.js',
], 'public/js/custom.js');//.version();

mix.combine([
    'resources/assets/custom/js/dashboard.js',
], 'public/js/dashboard.js');//.version();

/** TinyMCE **/
mix.combine([
    'node_modules/tinymce/tinymce.js',
    'node_modules/tinymce/jquery.tinymce.js',
    'node_modules/tinymce/themes/silver/theme.js',
], 'public/js/tinymcem.js');//.version();

mix.copy('node_modules/tinymce/plugins', 'public/js/plugins');
mix.copy('node_modules/tinymce/skins', 'public/js/skins');
mix.copy('node_modules/tinymce/icons/default/icons.js', 'public/js/icons/default/icons.js');


